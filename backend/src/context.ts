import { PrismaClient } from "@prisma/client";
import { PubSub } from "graphql-subscriptions";
import { Request } from "apollo-server";
import { db } from "./db";

const prisma = new PrismaClient();
const { JWT_SECRET } = process.env;

export interface Context {
  request: Request & any;
  prisma: PrismaClient;
  pubsub: PubSub;
  appSecret: string;
  db: PrismaClient;
}

const pubsub = new PubSub();

export function createContext(request: Request): Context {
  return {
    request,
    prisma,
    pubsub,
    appSecret: JWT_SECRET,
    db: db,
  };
}
