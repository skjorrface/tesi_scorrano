import needle from "needle";
const fs = require("fs");

async function getRequest(url) {
  const result = await needle("get", url);
  return result.body;
}

function saveHtml(html) {
  fs.writeFileSync("./src/crawler/data.html", html, { flag: "w" }, (err) => {
    if (err) console.log(err);
  });
}

export { getRequest, saveHtml };
