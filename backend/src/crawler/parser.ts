import cheerio from "cheerio";
const cheerioTableparser = require("cheerio-tableparser");

function parseAll(html) {
  let $ = cheerio.load(html);
  const h4 = $("h4").html();
  const serverTimeStamp = h4.substring(33);
  const myTableHtml = '<table id="mydata">' + $("table").html() + "</table>";
  $ = cheerio.load(myTableHtml);
  cheerioTableparser($);
  const data = $("#mydata").parsetable(true, true, false);
  data.splice(1, 1); // la cella "id" non ci interessa!
  data.forEach((el) => {
    el.splice(0, 1);
  });
  let finalData = new Array();
  // ordiniamo i dati come ci servono!
  for (let i = 0; i <= 94; i++) {
    console.log(
      "Elemento pushato: " + data[0][i] + " " + data[1][i] + " " + data[2][i] + " " + data[3][i] + " " + data[4][i]
    );
    finalData.push({
      station: data[0][i],
      temp: data[1][i],
      umid: data[2][i],
      vent: data[3][i],
      pio: data[4][i],
    });
  }
  return {
    data: finalData,
    timestamp: serverTimeStamp,
  };
}

export { parseAll };
