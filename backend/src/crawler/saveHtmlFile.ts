import { parse } from "nexus/dist/lib/package-json";
import { getRequest, saveHtml } from "./httpRequest";
import { parseAll } from "./parser";

async function saveHTML(url) {
  const html = await getRequest(url);
  saveHtml(html);
  return parseAll(html);
}

export { saveHTML };
