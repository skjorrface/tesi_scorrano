import * as path from "path";
import * as types from "./types";

import { makeSchema, declarativeWrappingPlugin } from "@nexus/schema";
import { nexusSchemaPrisma } from "nexus-plugin-prisma/schema";

export const schema = makeSchema({
  types,
  plugins: [nexusSchemaPrisma(), declarativeWrappingPlugin()],
  outputs: {
    schema: path.join(__dirname, "./../schema.graphql"),
    typegen: path.join(__dirname, "./generated/nexus.ts"),
  },
  typegenAutoConfig: {
    sources: [
      {
        source: `${__dirname.replace(/\/build$/, "/src")}/context.ts`,
        alias: "ContextModule",
      },
    ],
    contextType: "ContextModule.Context",
  },
});
