import { ApolloServer } from 'apollo-server-express';
import { createServer } from 'http';
import express from 'express';
import { schema } from './schema';
import { createContext } from './context';

const { PORT = 5000 } = process.env;

const app = express();
const server = createServer(app);
const apollo = new ApolloServer({ 
  context: createContext,
  schema
 });
apollo.applyMiddleware({ app });

server.listen({ port: PORT }, () => {
  process.stdout.write(`Il server sta funzionando al link http://localhost:${PORT}${apollo.graphqlPath}\n`);
});