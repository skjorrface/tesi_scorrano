import { objectType, extendType, stringArg, intArg } from "@nexus/schema";

const DataModel = objectType({
  name: "DataModel",
  definition(t) {
    t.int("id");
    t.int("dayDistance");
    t.string("dayDate"); // il giorno in cui viene effettuata la previsione!
    t.string("type"); // può essere solo "Forecast" o "Groundtruth"
    t.string("provider");
    t.float("MinTemp");
    t.float("MaxTemp");
    t.float("WindSpeed");
    t.string("WindDirection");
    t.string("tournamentIdentity");
    t.float("RainProbability");
    t.float("MmOfRain");
    t.float("Humidity");
  },
});

const getDataModel = extendType({
  type: "Query",
  definition(t) {
    t.field("getDataModel", {
      list: true,
      type: "DataModel",
      args: {
        tournamentIdentity: stringArg(),
      },
      async resolve(_root, args, ctx) {
        const DataModelForSingleTournament = await ctx.db.dataModel.findMany({
          where: {
            tournamentIdentity: args.tournamentIdentity,
          },
        });
        return DataModelForSingleTournament;
      },
    });
  },
});

export { DataModel, getDataModel };
