import { objectType } from "@nexus/schema";

const RmseModel = objectType({
  name: "RmseModel",
  definition(t) {
    t.int("id");
    t.int("dayDistance");
    t.string("dayDate"); // il giorno in cui viene effettuata la previsione!
    t.string("predictor"); // può essere solo "Forecast" o "Groundtruth"
    t.string("groundtruth");
    t.float("MinTempRmse");
    t.float("MaxTempRmse");
    t.float("WindSpeedRmse");
    t.string("WindDirectionRmse");
    t.string("tournamentIdentity");
    t.float("RainProbabilityRmse");
    t.float("MmOfRainRmse");
    t.float("HumidityRmse");
    t.float("CloudCoverRmse");
    t.float("totalRmse");
  },
});

export { RmseModel };
