import { objectType, extendType, stringArg, floatArg, intArg, booleanArg } from "@nexus/schema";
import { DataModel } from "./datamodel";
import { RmseModel } from "./rmsemodel";
import dayjs from "dayjs";
import { scheduler } from "../utils/scheduler/mainscheduler";
import utils from "../utils/miscellaneous";
import schedule from "node-schedule";

export const Location = objectType({
  name: "Location",
  definition(t) {
    t.string("display_name");
    t.string("lon");
    t.string("lat");
  },
});

export const Tournament = objectType({
  name: "Tournament",
  definition(t) {
    t.int("id");
    t.boolean("isInThePast");
    t.string("issuerIdentity"); // corrisponderà ad un expo token per rintracciare previsioni relative ad un utente già memorizzate nel database!
    t.string("issueDate"); // la data (in particolare il giorno), in cui è stata effettuata la richiesta delle previsioni!
    t.int("tournamentDuration"); // la durata del torneo in giorni!
    t.float("latitude");
    t.float("longitude");
    t.list.field("models", { type: DataModel });
    t.list.field("rmses", { type: RmseModel });
  },
});

export const createTournament = extendType({
  type: "Mutation",
  definition(t) {
    t.field("createTournament", {
      type: "Tournament",
      args: {
        issuerIdentity: stringArg(),
        tournamentDuration: intArg(),
        latitude: floatArg(),
        longitude: floatArg(),
      },
      async resolve(_root, args, ctx) {
        const newTournament = await ctx.db.tournament.create({
          data: {
            issuerIdentity: args.issuerIdentity,
            issueDate: dayjs().format("YYYY-MM-DD HH:mm"),
            tournamentDuration: args.tournamentDuration,
            latitude: args.latitude,
            longitude: args.longitude,
            isInThePast: false,
          },
        });
        scheduler(
          ctx,
          newTournament.issuerIdentity,
          newTournament.issueDate,
          args.tournamentDuration,
          args.latitude,
          args.longitude
        );
        return newTournament;
      },
    });
  },
});

export const getTournament = extendType({
  type: "Query",
  definition(t) {
    t.field("getTournament", {
      type: "Tournament",
      args: {
        issuerIdentity: stringArg(),
      },
      async resolve(_root, args, ctx) {
        const tournament = await ctx.db.tournament.findUnique({
          where: {
            issuerIdentity: args.issuerIdentity,
          },
          include: {
            models: true,
          },
        });
        return tournament;
      },
    });
  },
});

export const scheduless = extendType({
  type: "Query",
  definition(t) {
    t.field("scheduless", {
      type: "String",
      resolve(_root, args, ctx) {
        const rule = new schedule.RecurrenceRule();
        const now = dayjs();
        rule.date = parseInt(now.format("DD"));
        rule.hour = parseInt(now.format("HH"));
        rule.dayOfWeek = [0, 1, 2, 3, 4, 5, 6];
        let minutes = new Array();
        for (let i = 1; i <= 4; i++) {
          minutes.push(parseInt(now.add(i, "minute").format("mm")));
        }
        console.log(minutes);
        rule.minute = minutes;
        rule.second = 0;
        console.log(rule);

        const j = schedule.scheduleJob("myJob", rule, function () {
          if (parseInt(dayjs().format("mm")) == 6) {
            console.log("Job cancellato.");
            schedule.cancelJob("myJob");
          }
          console.log("Sono le: " + dayjs().format("HH:mm"));
        });
        return "OK";
      },
    });
  },
});

export const getCoords = extendType({
  type: "Query",
  definition(t) {
    t.field("getCoords", {
      type: "Location",
      args: {
        locationName: stringArg(),
      },
      async resolve(_root, args, ctx) {
        const coords = await utils.fromLocationNameToGPSCoords(args.locationName);
        return coords;
      },
    });
  },
});
