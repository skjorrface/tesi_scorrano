import { objectType, extendType } from "@nexus/schema";
import { saveHTML } from "../crawler/saveHtmlFile";
import schedule from "node-schedule";
import dayjs from "dayjs";

const TrentinoDaemon = objectType({
  name: "TrentinoDaemon",
  definition(t) {
    t.int("id");
    t.boolean("isActive");
  },
});

const DaemonStatus = extendType({
  type: "Query",
  definition(t) {
    t.field("daemonStatus", {
      type: "Boolean",
      async resolve(_root, args, ctx) {
        const daemonStatus = await ctx.db.trentinoDaemon.findMany();
        if (daemonStatus[0].isActive === true) return true;
        else return false;
      },
    });
  },
});

const DaemonToggle = extendType({
  type: "Mutation",
  definition(t) {
    t.field("daemonToggle", {
      type: "String",
      async resolve(_root, args, ctx) {
        let daemon = await ctx.db.trentinoDaemon.findMany();
        console.log(daemon);
        if (daemon.length === 0) {
          console.log("Non è stato istanziato alcun demone!");
          const newDaemon = await ctx.db.trentinoDaemon.create({
            data: {
              isActive: false,
            },
          });
          daemon = await ctx.db.trentinoDaemon.findMany();
        }
        if (daemon[0].isActive === true) {
          const fetchDaemon = schedule.scheduledJobs["fetchdaemon"];
          if (fetchDaemon !== undefined) fetchDaemon.cancel();
          const toggleOff = await ctx.db.trentinoDaemon.update({
            where: {
              id: daemon[0].id,
            },
            data: {
              isActive: false,
            },
          });
          return "Demone di Fetch fermato correttamente.";
        } else {
          const toggleOn = await ctx.db.trentinoDaemon.update({
            where: {
              id: daemon[0].id,
            },
            data: {
              isActive: true,
            },
          });
          const recurrenceRule = new schedule.RecurrenceRule();
          recurrenceRule.minute = 39;
          schedule.scheduleJob("fetchdaemon", recurrenceRule, async () => {
            const data = await saveHTML(`http://meteo.fmach.it/meteo/now.php`);
            const now = dayjs().subtract(39, "minute").format("DD-MM-YYYY HH:mm");
            if (data.timestamp !== now) {
              data.data.forEach(async (el) => {
                console.log("No!: " + data.timestamp + ", " + now);
                const model = await ctx.db.trentinoSchedule.create({
                  data: {
                    timestamp: data.timestamp,
                    didStationSendData: false,
                    station: {
                      connect: {
                        station: el.station.toLowerCase(),
                      },
                    },
                  },
                });
              });
            } else {
              const stations = await ctx.db.trentinoStation.findMany();
              data.data.forEach(
                async (el: { station: string; temp: string; umid: string; vent: string; pio: string }) => {
                  const hasDbStation = stations.filter(
                    (dbStation) => dbStation.station.toLowerCase() === el.station.toLowerCase()
                  );
                  if (hasDbStation.length === 0) return;
                  try {
                    const model = await ctx.db.trentinoSchedule.create({
                      data: {
                        timestamp: data.timestamp,
                        didStationSendData: true,
                        models: {
                          create: {
                            Provider: "Fmach",
                            station: el.station.toLowerCase(),
                            temp: el.temp != "" ? parseFloat(el.temp) : 0,
                            umid: el.umid != "" ? parseFloat(el.umid) : 0,
                            vent: el.vent != "" ? parseFloat(el.vent) : 0,
                            pio: el.pio != "" ? parseFloat(el.pio) : 0,
                          },
                        },
                        station: {
                          connect: {
                            station: el.station.toLowerCase(),
                          },
                        },
                      },
                    });
                    console.log(JSON.stringify(model));
                  } catch (err) {
                    console.log("E' qui che casca l'asino!: " + JSON.stringify(el));
                  }
                }
              );
            }
          });
        }
        return "Demone di fetch inizializzato.";
      },
    });
  },
});

export { TrentinoDaemon, DaemonToggle, DaemonStatus };
