import { objectType, extendType, stringArg } from "@nexus/schema";

const TrentinoData = objectType({
  name: "TrentinoData",
  definition(t) {
    t.int("id");
    t.int("houroffset");
    t.string("tournamentIdentity");
    t.string("tournamentIdentityplusOffset");
    t.list.field("models", { type: "TrentinoModel" });
  },
});

const TrentinoDataGet = extendType({
  type: "Query",
  definition(t) {
    t.list.field("getTrentinoData", {
      type: "TrentinoData",
      args: {
        tournamentIdentity: stringArg(),
      },
      async resolve(_root, args, ctx) {
        const trentinoData = await ctx.db.trentinoData.findMany({
          where: {
            tournamentIdentity: args.tournamentIdentity,
          },
          include: {
            models: true,
          },
        });
        return trentinoData;
      },
    });
  },
});

export { TrentinoData, TrentinoDataGet };
