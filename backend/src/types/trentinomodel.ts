import { objectType } from "@nexus/schema";

const TrentinoModel = objectType({
  name: "TrentinoModel",
  definition(t) {
    t.int("id");
    t.string("station");
    t.string("Provider");
    t.float("temp");
    t.float("umid");
    t.float("vent");
    t.float("pio");
  },
});

export { TrentinoModel };
