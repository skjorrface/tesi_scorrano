import { objectType, extendType, stringArg, intArg } from "@nexus/schema";

const TrentinoRmse = objectType({
  name: "TrentinoRmse",
  definition(t) {
    t.int("id");
    t.int("houroffset");
    t.string("tournamentIdentity");
    t.string("Provider");
    t.float("totalRmse");
    t.float("tempRmse");
    t.float("ventRmse");
    t.float("umidRmse");
    t.float("pioRmse");
  },
});

const GetTrentinoRmse = extendType({
  type: "Query",
  definition(t) {
    t.list.field("getTrentinoRmse", {
      type: "TrentinoRmse",
      args: {
        tournamentIdentity: stringArg(),
        houroffset: intArg(),
      },
      async resolve(_root, args, ctx) {
        const rmses = await ctx.db.trentinoRmse.findMany({
          where: {
            tournamentIdentity: args.tournamentIdentity,
            houroffset: args.houroffset,
          },
        });
        return rmses;
      },
    });
  },
});

const GetTrentinoRmseAll = extendType({
  type: "Query",
  definition(t) {
    t.list.field("getTrentinoRmseAll", {
      type: "TrentinoRmse",
      args: {
        tournamentIdentity: stringArg(),
      },
      async resolve(_root, args, ctx) {
        const rmses = await ctx.db.trentinoRmse.findMany({
          where: {
            tournamentIdentity: args.tournamentIdentity,
          },
        });
        return rmses;
      },
    });
  },
});

export { TrentinoRmse, GetTrentinoRmse, GetTrentinoRmseAll };
