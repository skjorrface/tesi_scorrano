import { objectType, extendType, stringArg, intArg } from "@nexus/schema";

const TrentinoSchedule = objectType({
  name: "TrentinoSchedule",
  definition(t) {
    t.int("id");
    t.int("stationIdentity");
    t.string("timestamp");
    t.boolean("didStationSendData");
    t.list.field("models", { type: "TrentinoModel" });
  },
});

const DeleteTrentinoSchedule = extendType({
  type: "Mutation",
  definition(t) {
    t.field("deleteTrentinoSchedule", {
      type: "TrentinoSchedule",
      args: {
        id: intArg(),
      },
      async resolve(_root, args, ctx) {
        const deleteTrentinoSchedule = await ctx.db.trentinoSchedule.delete({
          where: {
            id: args.id,
          },
        });
        return deleteTrentinoSchedule;
      },
    });
  },
});

const GetTrentinoSchedules = extendType({
  type: "Query",
  definition(t) {
    t.list.field("getTrentinoSchedules", {
      type: "TrentinoSchedule",
      args: {
        timestamp: stringArg(),
        station: stringArg(),
      },
      async resolve(_root, args, ctx) {
        if (args.timestamp === undefined && args.station === undefined)
          throw new Error("timestamp e station non possono essere entrambi indefiniti!");
        if (args.timestamp === undefined) {
          const station = await ctx.db.trentinoStation.findUnique({
            where: {
              station: args.station.toLowerCase(),
            },
          });
          if (station === null) throw new Error("Non esiste la stazione specificata!");
          const schedules = await ctx.db.trentinoSchedule.findMany({
            where: {
              stationIdentity: station.id,
            },
            include: {
              models: true,
            },
            take: 50,
          });
          return schedules;
        } else if (args.station === undefined) {
          const schedules = await ctx.db.trentinoSchedule.findMany({
            where: {
              timestamp: args.timestamp,
            },
            include: {
              models: true,
            },
            take: 50,
          });
          return schedules;
        } else {
          const station = await ctx.db.trentinoStation.findUnique({
            where: {
              station: args.station.toLowerCase(),
            },
          });
          const schedules = await ctx.db.trentinoSchedule.findMany({
            where: {
              stationIdentity: station.id,
              timestamp: args.timestamp,
            },
            include: {
              models: true,
            },
            take: 50,
          });
          return schedules;
        }
      },
    });
  },
});

export { TrentinoSchedule, GetTrentinoSchedules, DeleteTrentinoSchedule };
