import { objectType, extendType } from "@nexus/schema";
import { floatArg, resolveImportPath } from "@nexus/schema/dist/core";
import { stations } from "../utils/stations";

const TrentinoStation = objectType({
  name: "TrentinoStation",
  definition(t) {
    t.int("id");
    t.string("tournamentIdentity");
    t.string("station");
    t.float("longitude");
    t.float("latitude");
  },
});

const AddTrentinoStation = extendType({
  type: "Mutation",
  definition(t) {
    t.list.field("addTrentinoStation", {
      type: "TrentinoStation",
      // list: true, se ritrovi un errore guarda qui!
      async resolve(_root, args, ctx) {
        let addedStations = new Array();
        for (let i = 0; i < stations.length; i++) {
          const newStation = await ctx.db.trentinoStation.create({
            data: {
              station: stations[i].station.toLowerCase(),
              longitude: stations[i].longitude,
              latitude: stations[i].latitude,
            },
          });
          console.log(newStation);
          addedStations.push(newStation);
          await new Promise((resolve) => setTimeout(resolve, 3500));
        }
        return addedStations;
      },
    });
  },
});

const GetTrentinoStations = extendType({
  type: "Query",
  definition(t) {
    t.list.field("getTrentinoStation", {
      type: "TrentinoStation",
      async resolve(_root, args, ctx) {
        const stations = await ctx.db.trentinoStation.findMany();
        return stations;
      },
    });
  },
});

export { TrentinoStation, AddTrentinoStation, GetTrentinoStations };
