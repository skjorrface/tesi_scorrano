import { objectType, extendType, stringArg, floatArg, intArg, booleanArg } from "@nexus/schema";
import { TrentinoData } from "./trentinodata";
import { darksky_hourly } from "../utils/forecast/darksky";
import { openweathermap_hourly } from "../utils/forecast/openweathermap";
import { accuweather_hourly } from "../utils/forecast/accuweather";
import { climacell_hourly } from "../utils/forecast/climacell";
import dayjs from "dayjs";
import { fmachScheduler } from "../utils/scheduler/groundtruthscheduler";
import { TrentinoRmseScheduler } from "../utils/scheduler/rmsescheduler";
import { TrentinoRmse } from "./trentinormse";

export const TrentinoTournament = objectType({
  name: "TrentinoTournament",
  definition(t) {
    t.int("id");
    t.string("issuerIdentity"); // corrisponderà ad un expo token per rintracciare previsioni relative ad un utente già memorizzate nel database!
    t.string("issueDate"); // la data (in particolare il giorno), in cui è stata effettuata la richiesta delle previsioni!
    t.float("latitude");
    t.float("longitude");
    t.list.field("models", { type: TrentinoData });
    t.list.field("rmses", { type: TrentinoRmse });
  },
});

export const createTrentinoTournament = extendType({
  type: "Mutation",
  definition(t) {
    t.field("createTrentinoTournament", {
      type: "TrentinoTournament",
      args: {
        station: stringArg(),
        issuerIdentity: stringArg(),
      },
      async resolve(_root, args, ctx) {
        const findStation = await ctx.db.trentinoStation.findUnique({
          where: {
            station: args.station, // args.station.toLowerCase(),
          },
        });
        if (findStation === null) throw new Error("Non esiste la stazione specificata!");
        const newTournament = await ctx.db.trentinoTournament.create({
          data: {
            issuerIdentity: args.issuerIdentity,
            issueDate: dayjs().format("YYYY-MM-DD HH:mm:ss"),
            latitude: findStation.latitude,
            longitude: findStation.longitude,
          },
        });
        const climacell = await climacell_hourly(findStation.longitude, findStation.latitude);
        const darksky = await darksky_hourly(findStation.longitude, findStation.latitude);
        const openweathermap = await openweathermap_hourly(findStation.longitude, findStation.latitude);
        const accuweather = await accuweather_hourly(findStation.longitude, findStation.latitude);
        const forecasts = [climacell, darksky, openweathermap, accuweather];
        const providers = ["Climacell", "Darksky", "Openweathermap", "Accuweather"];
        for (let i = 0; i < 12; i++) {
          const TrentinoData = await ctx.db.trentinoTournament.update({
            where: {
              issuerIdentity: newTournament.issuerIdentity,
            },
            data: {
              models: {
                create: {
                  houroffset: i + 1,
                  tournamentIdentityplusOffset: newTournament.issuerIdentity.concat(String(i + 1)),
                  type: "Forecast",
                },
              },
            },
          });
          for (let j = 0; j <= 3; j++) {
            const TrentinoModel = await ctx.db.trentinoData.update({
              where: {
                tournamentIdentityplusOffset: newTournament.issuerIdentity.concat(String(i + 1)),
              },
              data: {
                models: {
                  create: {
                    station: args.station,
                    Provider: providers[j],
                    temp: forecasts[j][i].temp,
                    vent: forecasts[j][i].vent,
                    umid: forecasts[j][i].umid,
                    pio: forecasts[j][i].pio,
                  },
                },
              },
            });
          }
        }
        fmachScheduler(ctx, args.station, newTournament.issuerIdentity, newTournament.issueDate);
        TrentinoRmseScheduler(newTournament.issuerIdentity, ctx, newTournament.issueDate);
        return newTournament;
      },
    });
  },
});
