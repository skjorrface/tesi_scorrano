import axios from "axios";
import dayjs from "dayjs";

const accuweather_locationkey = async (lat, lon) => {
  let locationKey;
  const locationKeyReq = await axios
    .get("https://dataservice.accuweather.com/locations/v1/cities/geoposition/search", {
      params: {
        apikey: "LZxyOGyEorAhJpJad3aC0H2wXAX533RW",
        language: "it-it",
        toplevel: true, // vogliamo includere nel risultato la località con maggiore rilevanza
        q: `${lat},${lon}`,
      },
    })
    .then((data) => {
      locationKey = data.data.Key;
    });
  return locationKey;
};

export const accuweather_hourly = async (lon, lat) => {
  const locationKey = await accuweather_locationkey(lat, lon);
  const AccuweatherForecast = await axios.get(
    `http://dataservice.accuweather.com/forecasts/v1/hourly/12hour/${locationKey}`,
    {
      params: {
        apikey: "LZxyOGyEorAhJpJad3aC0H2wXAX533RW",
        language: "it-IT",
        details: true,
        metric: true, // altrimenti ci dà i gradi fahrenheit!
      },
    }
  );
  let AccuweatherHourly = new Array();
  AccuweatherForecast.data.forEach((el, index) => {
    if (index < 12) {
      AccuweatherHourly.push({
        temp: el.Temperature.Value,
        umid: el.RelativeHumidity,
        vent: parseFloat(el.Wind.Speed.Value) / 3.6, // in km/h
        pio: el.Rain.Value,
      });
    }
  });
  return AccuweatherHourly;
};

export const accuweather = async (lon: number, lat: number) => {
  try {
    const locationKey = await accuweather_locationkey(lat, lon);
    console.log("locationKey: " + locationKey);
    const AccuweatherForecast = await axios.get(
      `http://dataservice.accuweather.com/forecasts/v1/daily/5day/${locationKey}`,
      {
        params: {
          apikey: "LZxyOGyEorAhJpJad3aC0H2wXAX533RW",
          language: "it-IT",
          details: true,
          metric: true, // altrimenti ci dà i gradi fahrenheit!
        },
      }
    );
    const Forecasts = AccuweatherForecast.data.DailyForecasts.map((el, index) => {
      if (index <= 5) {
        const single_day_forecast = {
          dayDistance: index + 1,
          dayDate: dayjs.unix(el.EpochDate).add(1, "day").format("YYYY-MM-DD"),
          type: "Forecast",
          provider: "Accuweather",
          MinTemp: el.Temperature.Minimum.Value,
          MaxTemp: el.Temperature.Maximum.Value,
          WindSpeed: parseFloat(el.Day.Wind.Speed.Value) / 3.6,
          WindDirection: el.Day.Wind.Direction.Localized,
          RainProbability: el.Day.RainProbability,
          MmOfRain: el.Day.Rain.Value,
          CloudCover: el.Day.CloudCover,
          Humidity: 800.93,
        };
        return single_day_forecast;
      }
    });
    for (let i = 0; i < Forecasts.length; i++)
      if (Forecasts[i] === undefined) {
        Forecasts.splice(i, 1);
        i--;
      }
    return Forecasts;
  } catch (err) {
    throw new Error("Accuweather: " + err);
  }
};
