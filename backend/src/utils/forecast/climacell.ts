import axios from "axios";
import dayjs from "dayjs";
import utils from "../miscellaneous";

// Questa API richiede la data in formato ISO8601! Consultando la documentazione di dayjs
// ho trovato che bisogna usare: dayjs("2020-10-16").toISOString()

export const climacell_hourly = async (lon, lat) => {
  try {
    const ClimacellForecasts = await axios({
      url: `https://api.climacell.co/v3/weather/forecast/hourly?lat=${lat}&lon=${lon}&unit_system=si&start_time=now&fields=humidity,wind_speed,precipitation,temp&apikey=eF3vmwxFORpjoK5uN9Gu2V0eXiHuaEnN`,
    });
    let climacell_hourly = new Array();
    ClimacellForecasts.data.forEach((el, index) => {
      if (index >= 3 && index < 15) {
        climacell_hourly.push({
          temp: el.temp.value,
          umid: el.humidity.value,
          vent: el.wind_speed.value, // m/s
          pio: el.precipitation.value,
        });
      }
    });
    // console.log("Climacell: " + JSON.stringify(climacell_hourly));
    return climacell_hourly;
  } catch (err) {
    console.log(err);
  }
};

export const climacell = async (lon, lat) => {
  const oneday_date = dayjs().add(1, "day");
  const threeday_date = dayjs().add(3, "day");
  const fiveday_date = dayjs().add(5, "day");
  let oneday;
  let threeday;
  let fiveday;
  try {
    const climacell = await axios({
      url: `https://api.climacell.co/v3/weather/forecast/daily?apikey=eF3vmwxFORpjoK5uN9Gu2V0eXiHuaEnN&unit_system=si&start_time=${oneday_date.toISOString()}&lat=${lat}&lon=${lon}&fields=temp,precipitation,precipitation_probability,wind_speed,wind_direction,humidity`,
    });
    climacell.data.forEach((el) => {
      if (el.observation_time.value.includes(oneday_date.format("YYYY-MM-DD"))) oneday = el;
      if (el.observation_time.value.includes(threeday_date.format("YYYY-MM-DD"))) threeday = el;
      if (el.observation_time.value.includes(fiveday_date.format("YYYY-MM-DD"))) fiveday = el;
    });
    //console.log(climacell);
    const Forecasts = climacell.data.map((el, index) => {
      if (index <= 4) {
        const single_day_forecast = {
          dayDistance: index + 1,
          dayDate: el.observation_time.value,
          type: "Forecast",
          provider: "Climacell",
          MinTemp: parseFloat(el.temp[0].min.value),
          MaxTemp: parseFloat(el.temp[1].max.value),
          WindSpeed: (parseFloat(el.wind_speed[0].min.value) + parseFloat(el.wind_speed[1].max.value)) / 2,
          WindDirection: utils.fromDegToDirection(
            parseFloat(el.wind_direction[0].min.value + el.wind_direction[1].max.value) / 2
          ),
          RainProbability: el.precipitation_probability.value,
          Humidity: (el.humidity[0].min.value + el.humidity[1].max.value) / 2,
          MmOfRain: el.precipitation[0].max.value,
          CloudCover: 800.93, // non è disponibile quindi usiamo questo valore come SENTINELLA
        };
        return single_day_forecast;
      }
    });
    for (let i = 0; i < Forecasts.length; i++)
      if (Forecasts[i] === undefined) {
        Forecasts.splice(i, 1);
        i--;
      }
    return Forecasts;
  } catch (err) {
    throw new Error("Climacell: " + err);
  }
};
