import axios from "axios";
import dayjs from "dayjs";

export const darksky_hourly = async (lon: number, lat: number) => {
  const DarkskyForecast = await axios({
    url: `https://api.darksky.net/forecast/c4832df1c1208347d6df4833539b6e86/${lat},${lon}`,
    method: "GET",
    params: {
      units: "si",
    },
  });
  let DarkskyHourly = new Array();
  DarkskyForecast.data.hourly.data.forEach((el, index) => {
    if (index > 0 && index <= 12) {
      DarkskyHourly.push({
        temp: el.temperature,
        umid: el.humidity * 100,
        vent: parseFloat(el.windSpeed),
        pio: el.precipIntensity,
      });
    }
  });
  console.log("Darksky: " + JSON.stringify(DarkskyHourly));
  return DarkskyHourly;
};

export const darksky = async (lon: number, lat: number) => {
  // LA TIMEZONE DI QUESTO SERVER E' ETC/GMT, DOBBIAMO CONVERTIRLA IN GMT+02 PER OTTENERE I GIORNI GIUSTI
  // dopo aver consultato la documentazione la funzione da usare è dayjs.unix per ottenere una data valida dato l'epoch unix time!
  try {
    const DarkskyForecast = await axios({
      url: `https://api.darksky.net/forecast/c4832df1c1208347d6df4833539b6e86/${lat},${lon}`,
      method: "GET",
      params: {
        units: "si",
      },
    });
    const tomorrow = dayjs().add(1, "day");
    let startingIndex;
    DarkskyForecast.data.daily.data.forEach((el, index) => {
      if (tomorrow.format("YYYY-MM-DD") === dayjs.unix(el.time).format("YYYY-MM-DD")) startingIndex = index; // in questo modo capiamo da dove inizia il giorno successivo a quello di input dell'utente!
    });
    const Forecasts = DarkskyForecast.data.daily.data.map((el, index) => {
      if (index >= startingIndex && index <= startingIndex + 4) {
        const single_day_forecast = {
          dayDistance: index - startingIndex + 1,
          dayDate: dayjs.unix(el.time).format("YYYY-MM-DD"),
          type: "Forecast",
          provider: "Darksky",
          MinTemp: el.temperatureMin,
          MaxTemp: el.temperatureMax,
          WindSpeed: parseFloat(el.windSpeed),
          WindDirection: "N/D", // SEMBRA NON ESSERCI UN PARAMETRO DI DIREZIONE DEL VENTO NEI DATI!!!
          RainProbability: el.precipProbability * 100,
          MmOfRain: el.precipIntensity, // SIAMO SICURI SIANO I MM DI PIOGGIA?
          Humidity: el.humidity * 100,
          CloudCover: el.cloudCover,
        };
        return single_day_forecast;
      }
    });
    for (let i = 0; i < Forecasts.length; i++)
      if (Forecasts[i] === undefined) {
        Forecasts.splice(i, 1);
        i--;
      }
    return Forecasts;
  } catch (err) {
    throw new Error("Darksky: " + err);
  }
};
