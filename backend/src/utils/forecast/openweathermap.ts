import axios from "axios";
import dayjs from "dayjs";
import utils from "../miscellaneous";

export const openweathermap_hourly = async (lon: number, lat: number) => {
  try {
    const OpenweathermapForecast = await axios.get(
      `https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&units=metric&exclude=daily,minutely,current,alerts&appid=70b092a7d0430be4075c19602c858be2`
    );
    let OpenweatherHourly = new Array();
    OpenweathermapForecast.data.hourly.forEach((el, index) => {
      console.log("Pioggia: " + JSON.stringify(el.rain));
      if (index > 0 && index <= 12) {
        OpenweatherHourly.push({
          temp: el.temp,
          umid: el.humidity,
          vent: el.wind_speed,
          pio: parseInt(el.pop) === 0 ? 0 : el.rain !== undefined ? el.rain : 0,
        });
      }
    });
    return OpenweatherHourly;
  } catch (err) {
    console.log(err);
  }
};

export const openweathermap = async (lon: number, lat: number) => {
  try {
    const OpenweathermapForecast = await axios.get(
      `https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&units=metric&exclude=hourly,minutely,current,alerts&appid=70b092a7d0430be4075c19602c858be2`
    );
    console.log(OpenweathermapForecast.data.daily);
    const Forecasts = OpenweathermapForecast.data.daily.map((el, index) => {
      if (index !== 0 && index <= 5) {
        const single_day_forecast = {
          dayDistance: index,
          dayDate: dayjs.unix(el.dt).format("YYYY-MM-DD"),
          type: "Forecast",
          provider: "Openweathermap",
          MinTemp: el.temp.min,
          MaxTemp: el.temp.max,
          WindSpeed: el.wind_speed,
          WindDirection: utils.fromDegToDirection(el.wind_deg),
          RainProbability: el.pop * 100, // se è 800.93 vuol dire che non è un valore fornito dall'API
          MmOfRain: el.rain !== undefined ? el.rain : 0,
          Humidity: el.humidity,
          CloudCover: 800.93,
        };
        return single_day_forecast;
      }
    });
    for (let i = 0; i < Forecasts.length; i++)
      if (Forecasts[i] === undefined) {
        Forecasts.splice(i, 1);
        i--;
      }
    console.log(Forecasts);
    return Forecasts;
  } catch (err) {
    throw new Error("Openweathermap: " + err);
  }
};
