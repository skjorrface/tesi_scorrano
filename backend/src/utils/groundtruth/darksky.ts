import axios from "axios";
import dayjs from "dayjs";

export const darksky_gt = async (lon: number, lat: number, date: string) => {
  // si sta supponendo che la data sia in formato ISO YYYY-MM-DD
  // gt sta per ground truth ;-)
  // LA TIMEZONE DI QUESTO SERVER E' ETC/GMT, DOBBIAMO CONVERTIRLA IN GMT+02 PER OTTENERE I GIORNI GIUSTI
  // dopo aver consultato la documentazione la funzione da usare è dayjs.unix per ottenere una data valida dato l'epoch unix time!
  const today = dayjs();
  let Groundtruth;
  const DarkskyForecast = await axios({
    url: `https://api.darksky.net/forecast/c4832df1c1208347d6df4833539b6e86/${lat},${lon},${dayjs(date).unix()}`,
    method: "GET",
    params: {
      units: "si",
    },
  }).then((res) => {
    Groundtruth = {
      type: "Groundtruth",
      daydate: dayjs().format("YYYY-MM-DD"),
      provider: "Darksky",
      MinTemp: res.data.daily.data[0].temperatureMin,
      MaxTemp: res.data.daily.data[0].temperatureMax,
      WindSpeed: res.data.daily.data[0].windSpeed,
      WindDirection: "N/A",
      RainProbability: res.data.daily.data[0].precipProbability,
      MmOfRain: res.data.daily.data[0].precipIntensity,
      Humidity: res.data.daily.data[0].humidity,
      CloudCover: res.data.daily.data[0].cloudCover,
    };
  });
  return Groundtruth;
};
