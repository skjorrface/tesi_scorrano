import dayjs from "dayjs";
import axios from "axios";
export default {
  fromDateToCron: (date: string) => {
    const targetDate = dayjs(date);
    return {
      month: parseInt(targetDate.format("MM")),
      day: parseInt(targetDate.format("DD")),
      hours: parseInt(targetDate.format("H")),
      minutes: parseInt(targetDate.format("m")),
    };
  },
  fromLocationNameToGPSCoords: async (
    locationName: string
  ): Promise<{ display_name: string; lon: string; lat: string }> => {
    if (locationName === "") throw new Error("Il nome della località non può essere vuoto!");
    locationName.split(" ").join("%20"); // sostituiamo ogni singolo spazio con la sequenza di caratteri "%20" per non avere problemi con la query string!
    try {
      const coords = await axios.get(
        `https://eu1.locationiq.com/v1/search.php?key=397e7ebb225af6&country=it&city=${locationName}&format=json`
      );
      const lon = coords.data[0].lon.slice(0, -1);
      const lat = coords.data[0].lat.slice(0, -1);
      return {
        display_name: coords.data[0].display_name,
        lon: coords.data[0].lon.slice(0, -1),
        lat: coords.data[0].lat.slice(0, -1),
      };
    } catch (error) {
      console.log(error);
      return {
        display_name: "",
        lon: "",
        lat: "",
      };
    }
  },
  fromDegToDirection: (deg: number) => {
    if (deg > 11.25 && deg < 33.75) {
      return "NNE";
    } else if (deg > 33.75 && deg < 56.25) {
      return "ENE";
    } else if (deg > 56.25 && deg < 78.75) {
      return "E";
    } else if (deg > 78.75 && deg < 101.25) {
      return "ESE";
    } else if (deg > 101.25 && deg < 123.75) {
      return "ESE";
    } else if (deg > 123.75 && deg < 146.25) {
      return "SE";
    } else if (deg > 146.25 && deg < 168.75) {
      return "SSE";
    } else if (deg > 168.75 && deg < 191.25) {
      return "S";
    } else if (deg > 191.25 && deg < 213.75) {
      return "SSW";
    } else if (deg > 213.75 && deg < 236.25) {
      return "SW";
    } else if (deg > 236.25 && deg < 258.75) {
      return "WSW";
    } else if (deg > 258.75 && deg < 281.25) {
      return "W";
    } else if (deg > 281.25 && deg < 303.75) {
      return "WNW";
    } else if (deg > 303.75 && deg < 326.25) {
      return "NW";
    } else if (deg > 326.25 && deg < 348.75) {
      return "NNW";
    } else {
      return "N";
    }
  },
};
