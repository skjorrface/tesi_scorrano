import dayjs from "dayjs";
import schedule from "node-schedule";
import { darksky } from "./forecast/darksky";
import { openweathermap } from "./forecast/openweathermap";
import { accuweather } from "./forecast/accuweather";
import { climacell } from "./forecast/climacell";
import { darksky_gt } from "./groundtruth/darksky";

const javascriptDateFromDayjsDate = (dayjsDate) => {
  const targetDate = dayjs(dayjsDate);
  return {
    year: parseInt(targetDate.format("YYYY")),
    month: parseInt(targetDate.format("M")) - 1,
    day: parseInt(targetDate.format("D")),
    hour: parseInt(targetDate.format("H")),
    minute: parseInt(targetDate.format("m")) + 1,
    second: parseInt(targetDate.format("s")),
  };
};

const GroundtruthScheduler = (issuerIdentity, ctx, date, latitude, longitude) => {
  const forecastday = javascriptDateFromDayjsDate(date);
  const oneDayDate = new Date(forecastday.year, forecastday.month, forecastday.day + 2, 0, 0, 30);
  const threeDayDate = new Date(forecastday.year, forecastday.month, forecastday.day + 4, 0, 0, 30);
  const fiveDayDate = new Date(forecastday.year, forecastday.month, forecastday.day + 6, 0, 0, 30);
  console.log("Groundtruth a un giorno schedulata per...: " + dayjs(oneDayDate).format("YYYY-MM-DD HH:mm:ss"));
  console.log("Groundtruth a tre giorni schedulata per...: " + dayjs(threeDayDate).format("YYYY-MM-DD HH:mm:ss"));
  console.log("Groundtruth a cinque giorni schedulata per...: " + dayjs(fiveDayDate).format("YYYY-MM-DD HH:mm:ss"));
  schedule.scheduleJob(oneDayDate, async () => {
    const darksky_gtruth = await darksky_gt(longitude, latitude, dayjs(date).add(1, "day").format("YYYY-MM-DD"));
    const oneDay = await ctx.db.tournament.update({
      where: {
        issuerIdentity,
      },
      data: {
        models: {
          create: {
            dayDistance: 1,
            dayDate: dayjs(date).add(2, "day").format("YYYY-MM-DD"),
            type: "Groundtruth",
            provider: "Darksky",
            MinTemp: darksky_gtruth.MinTemp,
            MaxTemp: darksky_gtruth.MaxTemp,
            WindSpeed: darksky_gtruth.WindSpeed,
            WindDirection: darksky_gtruth.WindDirection,
            RainProbability: darksky_gtruth.RainProbability,
            MmOfRain: darksky_gtruth.MmOfRain,
            CloudCover: darksky_gtruth.CloudCover,
            Humidity: darksky_gtruth.Humidity,
          },
        },
      },
    });
  });
  schedule.scheduleJob(oneDayDate, async () => {
    const darksky_gtruth = await darksky_gt(longitude, latitude, dayjs(date).add(3, "day").format("YYYY-MM-DD"));
    const threeDay = await ctx.db.tournament.update({
      where: {
        issuerIdentity,
      },
      data: {
        models: {
          create: {
            dayDistance: 1,
            dayDate: dayjs(date).add(4, "day").format("YYYY-MM-DD"),
            type: "Groundtruth",
            provider: "Darksky",
            MinTemp: darksky_gtruth.MinTemp,
            MaxTemp: darksky_gtruth.MaxTemp,
            WindSpeed: darksky_gtruth.WindSpeed,
            WindDirection: darksky_gtruth.WindDirection,
            RainProbability: darksky_gtruth.RainProbability,
            MmOfRain: darksky_gtruth.MmOfRain,
            CloudCover: darksky_gtruth.CloudCover,
            Humidity: darksky_gtruth.Humidity,
          },
        },
      },
    });
  });
  schedule.scheduleJob(oneDayDate, async () => {
    const darksky_gtruth = await darksky_gt(longitude, latitude, dayjs(date).add(5, "day").format("YYYY-MM-DD"));
    const fiveDay = await ctx.db.tournament.update({
      where: {
        issuerIdentity,
      },
      data: {
        models: {
          create: {
            dayDistance: 1,
            dayDate: dayjs(date).add(6, "day").format("YYYY-MM-DD"),
            type: "Groundtruth",
            provider: "Darksky",
            MinTemp: darksky_gtruth.MinTemp,
            MaxTemp: darksky_gtruth.MaxTemp,
            WindSpeed: darksky_gtruth.WindSpeed,
            WindDirection: darksky_gtruth.WindDirection,
            RainProbability: darksky_gtruth.RainProbability,
            MmOfRain: darksky_gtruth.MmOfRain,
            CloudCover: darksky_gtruth.CloudCover,
            Humidity: darksky_gtruth.Humidity,
          },
        },
      },
    });
  });
};

const ForecastScheduler = (issuerIdentity, ctx, date, latitude, longitude) => {
  const javascriptDate = javascriptDateFromDayjsDate(date);
  const finalDate = new Date(
    javascriptDate.year,
    javascriptDate.month,
    javascriptDate.day,
    javascriptDate.hour,
    javascriptDate.minute,
    javascriptDate.second
  );
  console.log("Scheduling fissato per: " + finalDate);
  schedule.scheduleJob(finalDate, async () => {
    console.log("Eseguendo il job...");
    const darksky_fc = await darksky(longitude, latitude);
    const accuweather_fc = await accuweather(longitude, latitude);
    const climacell_fc = await climacell(longitude, latitude);
    const openweathermap_fc = await openweathermap(longitude, latitude);
    const forecasts = [darksky_fc, accuweather_fc, climacell_fc, openweathermap_fc];
    forecasts.forEach(async (forecast, i) => {
      let forecast_service;
      if (i === 0) forecast_service = "Darksky";
      else if (i === 1) forecast_service = "Accuweather";
      else if (i === 2) forecast_service = "Climacell";
      else if (i === 3) forecast_service = "Openweathermap";
      const oneDay = await ctx.db.tournament.update({
        where: {
          issuerIdentity,
        },
        data: {
          models: {
            create: {
              dayDistance: 1,
              dayDate: dayjs(date).format("YYYY-MM-DD"),
              type: "Forecast",
              provider: forecast_service,
              MinTemp: forecast[0].MinTemp,
              MaxTemp: forecast[0].MaxTemp,
              WindSpeed: forecast[0].WindSpeed,
              WindDirection: forecast[0].WindDirection,
              RainProbability: forecast[0].RainProbability,
              MmOfRain: forecast[0].MmOfRain,
              CloudCover: forecast[0].CloudCover,
              Humidity: forecast[0].Humidity,
            },
          },
        },
      });
      const threeDay = await ctx.db.tournament.update({
        where: {
          issuerIdentity,
        },
        data: {
          models: {
            create: {
              dayDistance: 3,
              dayDate: dayjs(date).format("YYYY-MM-DD"),
              type: "Forecast",
              provider: forecast_service,
              MinTemp: forecast[2].MinTemp,
              MaxTemp: forecast[2].MaxTemp,
              WindSpeed: forecast[2].WindSpeed,
              WindDirection: forecast[2].WindDirection,
              RainProbability: forecast[2].RainProbability,
              MmOfRain: forecast[2].MmOfRain,
              CloudCover: forecast[2].CloudCover,
              Humidity: forecast[2].Humidity,
            },
          },
        },
      });
      const fiveDay = await ctx.db.tournament.update({
        where: {
          issuerIdentity,
        },
        data: {
          models: {
            create: {
              dayDistance: 5,
              dayDate: dayjs(date).format("YYYY-MM-DD"),
              type: "Forecast",
              provider: forecast_service,
              MinTemp: forecast[4].MinTemp,
              MaxTemp: forecast[4].MaxTemp,
              WindSpeed: forecast[4].WindSpeed,
              WindDirection: forecast[4].WindDirection,
              RainProbability: forecast[4].RainProbability,
              MmOfRain: forecast[4].MmOfRain,
              CloudCover: forecast[4].CloudCover,
              Humidity: forecast[4].Humidity,
            },
          },
        },
      });
    });
  });
};

const scheduler = (ctx, issuerIdentity, startDate, tournamentDuration, latitude, longitude) => {
  // supponiamo startDate sia già correttamente formattato! Una semplice stringa.
  for (let i = 0; i <= tournamentDuration; i++) {
    const targetDate = dayjs(startDate).add(i, "day");
    ForecastScheduler(issuerIdentity, ctx, targetDate, latitude, longitude);
    GroundtruthScheduler(issuerIdentity, ctx, targetDate, latitude, longitude);
  }
};

export { scheduler };
