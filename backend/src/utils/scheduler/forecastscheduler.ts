import dayjs from "dayjs";
import schedule from "node-schedule";
import { darksky } from "../forecast/darksky";
import { openweathermap } from "../forecast/openweathermap";
import { accuweather } from "../forecast/accuweather";
import { climacell } from "../forecast/climacell";

const ForecastScheduler = (
  issuerIdentity: string,
  tournamentDay: number,
  date: string,
  ctx,
  latitude: number,
  longitude: number
) => {
  const finalDate = new Date(dayjs(date).format("YYYY-MM-DD") + "T" + dayjs(date).add(1, "minute").format("HH:mm:ss")); // FAI MOLTA ATTENZIONE QUI, FORSE E' SOLO UN MINUTO NON UN GIORNO
  console.log(finalDate);
  console.log("Scheduling fissato per: " + finalDate);
  schedule.scheduleJob(finalDate, async () => {
    console.log("Eseguendo il job...");
    const darksky_fc = await darksky(longitude, latitude);
    const accuweather_fc = await accuweather(longitude, latitude);
    const climacell_fc = await climacell(longitude, latitude);
    const openweathermap_fc = await openweathermap(longitude, latitude);
    const forecasts = [darksky_fc, accuweather_fc, climacell_fc, openweathermap_fc];
    forecasts.forEach(async (forecast, i) => {
      let forecast_service;
      if (i === 0) forecast_service = "Darksky";
      else if (i === 1) forecast_service = "Accuweather";
      else if (i === 2) forecast_service = "Climacell";
      else if (i === 3) forecast_service = "Openweathermap";
      const oneDay = await ctx.db.tournament.update({
        where: {
          issuerIdentity,
        },
        data: {
          models: {
            create: {
              dayDistance: 1,
              tournamentDay,
              dayDate: dayjs(date).format("YYYY-MM-DD"),
              type: "Forecast",
              provider: forecast_service,
              MinTemp: forecast[0].MinTemp,
              MaxTemp: forecast[0].MaxTemp,
              WindSpeed: forecast[0].WindSpeed,
              WindDirection: forecast[0].WindDirection,
              RainProbability: forecast[0].RainProbability,
              MmOfRain: forecast[0].MmOfRain,
              CloudCover: forecast[0].CloudCover,
              Humidity: forecast[0].Humidity,
            },
          },
        },
      });
      const threeDay = await ctx.db.tournament.update({
        where: {
          issuerIdentity,
        },
        data: {
          models: {
            create: {
              dayDistance: 3,
              tournamentDay,
              dayDate: dayjs(date).format("YYYY-MM-DD"),
              type: "Forecast",
              provider: forecast_service,
              MinTemp: forecast[2].MinTemp,
              MaxTemp: forecast[2].MaxTemp,
              WindSpeed: forecast[2].WindSpeed,
              WindDirection: forecast[2].WindDirection,
              RainProbability: forecast[2].RainProbability,
              MmOfRain: forecast[2].MmOfRain,
              CloudCover: forecast[2].CloudCover,
              Humidity: forecast[2].Humidity,
            },
          },
        },
      });
      const fiveDay = await ctx.db.tournament.update({
        where: {
          issuerIdentity,
        },
        data: {
          models: {
            create: {
              dayDistance: 5,
              tournamentDay,
              dayDate: dayjs(date).format("YYYY-MM-DD"),
              type: "Forecast",
              provider: forecast_service,
              MinTemp: forecast[4].MinTemp,
              MaxTemp: forecast[4].MaxTemp,
              WindSpeed: forecast[4].WindSpeed,
              WindDirection: forecast[4].WindDirection,
              RainProbability: forecast[4].RainProbability,
              MmOfRain: forecast[4].MmOfRain,
              CloudCover: forecast[4].CloudCover,
              Humidity: forecast[4].Humidity,
            },
          },
        },
      });
    });
  });
};

export { ForecastScheduler };
