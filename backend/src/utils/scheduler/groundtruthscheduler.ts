import dayjs from "dayjs";
import schedule from "node-schedule";
import { darksky_gt } from "../groundtruth/darksky";
import { saveHTML } from "../../crawler/saveHtmlFile";

const fmachScheduler = async (ctx, stationName: string, tournamentIdentity: string, issueDate: string) => {
  const fmachRule = new schedule.RecurrenceRule();
  fmachRule.minute = dayjs(issueDate).minute();
  fmachRule.second = dayjs(issueDate).second();
  schedule.scheduleJob(tournamentIdentity, fmachRule, async () => {
    const now = dayjs();
    const diff = now.diff(dayjs(issueDate), "hour");
    if (diff > 12) {
      schedule.cancelJob(tournamentIdentity);
      return;
    }
    console.log("Ecco la diff: " + diff);
    const data = await saveHTML(`http://meteo.fmach.it/meteo/now.php`);
    data.data.forEach(async (el) => {
      if (el.station.toLowerCase() === stationName.toLowerCase()) {
        console.log("Trovato!");
        const newAdd = await ctx.db.trentinoData.update({
          where: {
            tournamentIdentityplusOffset: tournamentIdentity.concat(String(diff)),
          },
          data: {
            models: {
              create: {
                Provider: "Fmach",
                station: el.station,
                temp: el.temp != "" ? parseFloat(el.temp) : 0,
                umid: el.umid != "" ? parseFloat(el.umid) : 0,
                vent: el.vent != "" ? parseFloat(el.vent) : 0,
                pio: el.pio != "" ? parseFloat(el.pio) : 0,
              },
            },
          },
        });
      }
    });
  });
};

const GroundtruthScheduler = (issuerIdentity, tournamentDay, date, ctx, latitude, longitude) => {
  const forecastday = dayjs(date).format("YYYY-MM-DD");
  const oneDayDate = new Date(dayjs(forecastday).add(2, "minute").format("YYYY-MM-DD") + "T00:00:30");
  const threeDayDate = new Date(dayjs(forecastday).add(4, "minute").format("YYYY-MM-DD") + "T00:00:30");
  const fiveDayDate = new Date(dayjs(forecastday).add(6, "minute").format("YYYY-MM-DD") + "T00:00:30");
  console.log("Groundtruth a un giorno schedulata per...: " + dayjs(oneDayDate).format("YYYY-MM-DD HH:mm:ss"));
  console.log("Groundtruth a tre giorni schedulata per...: " + dayjs(threeDayDate).format("YYYY-MM-DD HH:mm:ss"));
  console.log("Groundtruth a cinque giorni schedulata per...: " + dayjs(fiveDayDate).format("YYYY-MM-DD HH:mm:ss"));
  schedule.scheduleJob(oneDayDate, async () => {
    const darksky_gtruth = await darksky_gt(longitude, latitude, dayjs(date).add(2, "minute").format("YYYY-MM-DD"));
    const oneDay = await ctx.db.tournament.update({
      where: {
        issuerIdentity,
      },
      data: {
        models: {
          create: {
            dayDistance: 1,
            dayDate: dayjs(date).add(2, "day").format("YYYY-MM-DD"),
            tournamentDay,
            type: "Groundtruth",
            provider: "Darksky",
            MinTemp: darksky_gtruth.MinTemp,
            MaxTemp: darksky_gtruth.MaxTemp,
            WindSpeed: darksky_gtruth.WindSpeed,
            WindDirection: darksky_gtruth.WindDirection,
            RainProbability: darksky_gtruth.RainProbability,
            MmOfRain: darksky_gtruth.MmOfRain,
            CloudCover: darksky_gtruth.CloudCover,
            Humidity: darksky_gtruth.Humidity,
          },
        },
      },
    });
  });
  schedule.scheduleJob(threeDayDate, async () => {
    const darksky_gtruth = await darksky_gt(longitude, latitude, dayjs(date).add(4, "day").format("YYYY-MM-DD"));
    const threeDay = await ctx.db.tournament.update({
      where: {
        issuerIdentity,
      },
      data: {
        models: {
          create: {
            dayDistance: 3,
            dayDate: dayjs(date).add(4, "day").format("YYYY-MM-DD"),
            tournamentDay,
            type: "Groundtruth",
            provider: "Darksky",
            MinTemp: darksky_gtruth.MinTemp,
            MaxTemp: darksky_gtruth.MaxTemp,
            WindSpeed: darksky_gtruth.WindSpeed,
            WindDirection: darksky_gtruth.WindDirection,
            RainProbability: darksky_gtruth.RainProbability,
            MmOfRain: darksky_gtruth.MmOfRain,
            CloudCover: darksky_gtruth.CloudCover,
            Humidity: darksky_gtruth.Humidity,
          },
        },
      },
    });
  });
  schedule.scheduleJob(fiveDayDate, async () => {
    const darksky_gtruth = await darksky_gt(longitude, latitude, dayjs(date).add(6, "day").format("YYYY-MM-DD"));
    const fiveDay = await ctx.db.tournament.update({
      where: {
        issuerIdentity,
      },
      data: {
        models: {
          create: {
            dayDistance: 5,
            dayDate: dayjs(date).add(6, "day").format("YYYY-MM-DD"),
            tournamentDay,
            type: "Groundtruth",
            provider: "Darksky",
            MinTemp: darksky_gtruth.MinTemp,
            MaxTemp: darksky_gtruth.MaxTemp,
            WindSpeed: darksky_gtruth.WindSpeed,
            WindDirection: darksky_gtruth.WindDirection,
            RainProbability: darksky_gtruth.RainProbability,
            MmOfRain: darksky_gtruth.MmOfRain,
            CloudCover: darksky_gtruth.CloudCover,
            Humidity: darksky_gtruth.Humidity,
          },
        },
      },
    });
  });
};

export { GroundtruthScheduler, fmachScheduler };
