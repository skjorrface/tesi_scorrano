import { GroundtruthScheduler } from "./groundtruthscheduler";
import { ForecastScheduler } from "./forecastscheduler";
import { RmseScheduler } from "./rmsescheduler";
import dayjs from "dayjs";

const scheduler = (ctx, issuerIdentity, startDate, tournamentDuration, latitude, longitude) => {
  // supponiamo startDate sia già correttamente formattato! Una semplice stringa.
  for (let i = 0; i < tournamentDuration; i++) {
    const targetDate = dayjs(startDate).add(i, "day");
    ForecastScheduler(issuerIdentity, i + 1, targetDate.format("YYYY-MM-DD HH:mm"), ctx, latitude, longitude);
    GroundtruthScheduler(issuerIdentity, i + 1, targetDate, ctx, latitude, longitude);
    RmseScheduler(issuerIdentity, ctx, targetDate);
  }
};

export { scheduler };
