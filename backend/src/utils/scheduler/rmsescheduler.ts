import dayjs from "dayjs";
import schedule from "node-schedule";
import RMSE from "rmse";

export const TrentinoRmseScheduler = async (issuerIdentity, ctx, date) => {
  const rmseID: string = issuerIdentity + "rmse";
  const rmseRule = new schedule.RecurrenceRule();
  rmseRule.minute = dayjs(date).add(1, "minute").minute();
  rmseRule.second = dayjs(date).second();
  schedule.scheduleJob(rmseID, rmseRule, async () => {
    const now = dayjs().subtract(1, "minute");
    const diff = now.diff(dayjs(date), "hour");
    console.log("Diff nella rmse: " + diff);
    if (diff > 12) {
      schedule.cancelJob(rmseID);
      return;
    }
    if (diff === 0) {
      console.log("In questo caso non se ne fa niente!");
      return;
    }
    const tournament = await ctx.db.trentinoData.findMany({
      where: {
        tournamentIdentity: issuerIdentity,
        houroffset: diff,
      },
      select: {
        models: true,
      },
    });
    console.log(tournament);
    let Groundtruth;
    tournament[0].models.forEach((el, index) => {
      if (el.Provider == "Fmach") Groundtruth = el;
      console.log(Groundtruth);
    });
    const providers = ["Darksky", "Openweathermap", "Accuweather", "Climacell"];
    providers.forEach((provider) => {
      tournament[0].models.forEach(async (forecast) => {
        if (provider === forecast.Provider) {
          const tempDS = [
            {
              actual: Groundtruth.temp,
              predicted: forecast.temp,
            },
          ];
          const ventDS = [
            {
              actual: Groundtruth.vent,
              predicted: forecast.vent,
            },
          ];
          const umidDS = [
            {
              actual: Groundtruth.umid,
              predicted: forecast.umid,
            },
          ];
          const pioDS = [
            {
              actual: Groundtruth.pio,
              predicted: forecast.pio,
            },
          ];
          const totalDS = tempDS.concat(ventDS, umidDS, pioDS);
          const newRmse = await ctx.db.trentinoTournament.update({
            where: {
              issuerIdentity,
            },
            data: {
              rmses: {
                create: {
                  Provider: provider,
                  houroffset: diff,
                  totalRmse: RMSE.rmse(totalDS),
                  tempRmse: RMSE.rmse(tempDS),
                  ventRmse: RMSE.rmse(ventDS),
                  umidRmse: RMSE.rmse(umidDS),
                  pioRmse: RMSE.rmse(pioDS),
                },
              },
            },
          });
        }
      });
    });
  });
};

export const RmseScheduler = (issuerIdentity, ctx, date) => {
  const forecastday = dayjs(date).format("YYYY-MM-DD");
  const oneDayDate = new Date(dayjs(forecastday).add(2, "day").format("YYYY-MM-DD") + "T00:01:00");
  const threeDayDate = new Date(dayjs(forecastday).add(4, "day").format("YYYY-MM-DD") + "T00:01:00");
  const fiveDayDate = new Date(dayjs(forecastday).add(6, "day").format("YYYY-MM-DD") + "T00:01:00");

  console.log("Calcolo rmse a un giorno schedulata per...: " + dayjs(oneDayDate).format("YYYY-MM-DD HH:mm:ss"));
  console.log("Calcolo rmse a tre giorni schedulata per...: " + dayjs(threeDayDate).format("YYYY-MM-DD HH:mm:ss"));
  console.log("Calcolo rmse a cinque giorni schedulata per...: " + dayjs(fiveDayDate).format("YYYY-MM-DD HH:mm:ss"));
  const predictors = ["Darksky", "Climacell", "Accuweather", "Openweathermap"];

  for (let k = 2; k <= 6; k += 2) {
    schedule.scheduleJob(k == 2 ? oneDayDate : k == 4 ? threeDayDate : fiveDayDate, async () => {
      let Forecasts = new Array();
      const GroundTruth = await ctx.db.dataModel.findMany({
        where: {
          type: "Groundtruth",
          provider: "Darksky",
          dayDate: forecastday,
          tournamentIdentity: issuerIdentity,
        },
      });
      for (let i = 0; i < predictors.length; i++) {
        const forecast = await ctx.db.dataModel.findMany({
          where: {
            type: "Forecast",
            provider: predictors[i],
            tournamentIdentity: issuerIdentity,
            dayDate: dayjs(GroundTruth.dayDate).subtract(k, "day").format("YYYY-MM-DD"),
          },
        });
        Forecasts.push(forecast);
      }
      for (let j = 0; j < Forecasts.length; j++) {
        const MinTempDS = {
          actual: GroundTruth[0].MinTemp,
          predicted: Forecasts[j].MinTemp,
        };
        const MaxTempDS = {
          actual: GroundTruth[0].MaxTemp,
          predicted: Forecasts[j].MaxTemp,
        };
        const WindSpeedDS = {
          actual: GroundTruth[0].WindSpeed,
          predicted: Forecasts[j].WindSpeed,
        };
        const WindDirectionDS = {
          actual: GroundTruth[0].WindDirection,
          predicted: Forecasts[j].WindDirection,
        };
        const RainProbabilityDS = {
          actual: GroundTruth[0].RainProbability,
          predicted: Forecasts[j].RainProbability,
        };
        const MmOfRainDS = {
          actual: GroundTruth[0].MmOfRain,
          predicted: Forecasts[j].MmOfRain,
        };
        const HumidityDS = {
          actual: GroundTruth[0].Humidity,
          predicted: Forecasts[j].Humidity,
        };
        const CloudCoverDS = {
          actual: GroundTruth[0].CloudCover,
          predicted: Forecasts[j].CloudCover,
        };
        const TotalDS = [
          MinTempDS,
          MaxTempDS,
          WindSpeedDS,
          WindDirectionDS,
          RainProbabilityDS,
          MmOfRainDS,
          HumidityDS,
          CloudCoverDS,
        ];
        const rmsemodel = await ctx.db.tournament.update({
          where: {
            issuerIdentity,
          },
          data: {
            rmses: {
              create: {
                dayDate: dayjs(forecastday).add(k, "day").format("YYYY-MM-DD"),
                dayDistance: k - 1,
                predictor: predictors[j],
                groundtruth: "Darksky",
                totalRmse: RMSE.rmse(TotalDS) || 0,
                MinTempRmse: RMSE.rmse(MinTempDS) || 0,
                MaxTempRmse: RMSE.rmse(MaxTempDS) || 0,
                WindSpeedRmse: RMSE.rmse(WindSpeedDS) || 0,
                WindDirectionRmse: RMSE.rmse(WindDirectionDS) || 0,
                RainProbabilityRmse: RMSE.rmse(RainProbabilityDS) || 0,
                MmOfRainRmse: RMSE.rmse(MmOfRainDS) || 0,
                HumidityRmse: RMSE.rmse(HumidityDS) || 0,
                CloudCoverRmse: RMSE.rmse(CloudCoverDS) || 0,
              },
            },
          },
        });
      }
    });
  }
};
