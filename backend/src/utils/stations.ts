const stations = [
  {
    station: "Ala",
    latitude: 45.786148,
    longitude: 45.786148,
  },
  {
    station: "Aldeno",
    latitude: 45.991325,
    longitude: 11.10507,
  },
  {
    station: "Arco",
    latitude: 45.9104,
    longitude: 10.88716,
  },
  {
    station: "Arsio",
    latitude: 46.425854,
    longitude: 11.096618,
  },
  {
    station: "Avio",
    latitude: 45.736847,
    longitude: 10.944334,
  },
  {
    station: "Banco-Casez",
    latitude: 46.371399,
    longitude: 11.06169,
  },
  {
    station: "Baselga di Pine",
    latitude: 46.125839,
    longitude: 11.251293,
  },
  {
    station: "Besagno",
    latitude: 45.836788,
    longitude: 10.975345,
  },
  {
    station: "Besenello",
    latitude: 45.940849,
    longitude: 11.101557,
  },
  {
    station: "Bezzecca",
    latitude: 45.895657,
    longitude: 10.725044,
  },
  {
    station: "Bleggio Superiore",
    latitude: 46.025211,
    longitude: 10.8368,
  },
  {
    station: "Borgo Valsugana",
    latitude: 46.045193,
    longitude: 11.47508,
  },
  {
    station: "Brancolino",
    latitude: 45.898945,
    longitude: 11.021172,
  },
  {
    station: "Brez",
    latitude: 46.433762,
    longitude: 11.108862,
  },
  {
    station: "Caldes",
    latitude: 46.378632,
    longitude: 10.960859,
  },
  {
    station: "Caldonazzo",
    latitude: 45.998245,
    longitude: 11.269902,
  },
  {
    station: "Cavedine",
    latitude: 45.985001,
    longitude: 10.981014,
  },
  {
    station: "Cembra",
    latitude: 46.166027,
    longitude: 11.226392,
  },
  {
    station: "Cimone",
    latitude: 45.980736,
    longitude: 11.073576,
  },
  {
    station: "Cles",
    latitude: 46.361172,
    longitude: 11.039959,
  },
  {
    station: "Cognola",
    latitude: 46.079643,
    longitude: 11.133946,
  },
  {
    station: "Coredo",
    latitude: 46.344395,
    longitude: 11.085845,
  },
  {
    station: "Cunevo",
    latitude: 46.280739,
    longitude: 11.040498,
  },
  {
    station: "Denno",
    latitude: 46.266727,
    longitude: 11.056045,
  },
  {
    station: "Dercolo",
    latitude: 46.248516,
    longitude: 11.048683,
  },
  {
    station: "Dro",
    latitude: 45.954475,
    longitude: 10.910038,
  },
  {
    station: "Faedo - Maso Togn",
    latitude: 46.194763,
    longitude: 11.169777,
  },
  {
    station: "Flavon",
    latitude: 46.295692,
    longitude: 11.037565,
  },
  {
    station: "Fondo",
    latitude: 46.437698,
    longitude: 11.129643,
  },
  {
    station: "Gardolo",
    latitude: 46.112576,
    longitude: 11.100472,
  },
  {
    station: "Giovo - Bosch",
    latitude: 46.15818,
    longitude: 11.127445,
  },
  {
    station: "Lavaze",
    latitude: 46.358234,
    longitude: 11.494065,
  },
  {
    station: "Lavis",
    latitude: 46.139389,
    longitude: 11.097886,
  },
  {
    station: "Levico",
    latitude: 46.018063,
    longitude: 11.342454,
  },
  {
    station: "Livo",
    latitude: 46.405174,
    longitude: 11.019969,
  },
  {
    station: "Lomaso",
    latitude: 46.014465,
    longitude: 10.866802,
  },
  {
    station: "Loppio",
    latitude: 45.852062,
    longitude: 10.938546,
  },
  {
    station: "Malga Flavona",
    latitude: 46.220734,
    longitude: 10.930422,
  },
  {
    station: "Mama di Avio",
    latitude: 45.705288,
    longitude: 10.926449,
  },
  {
    station: "Marco",
    latitude: 45.845894,
    longitude: 11.007034,
  },
  {
    station: "Maso Callianer",
    latitude: 46.158905,
    longitude: 11.103648,
  },
  {
    station: "Meano-Cortesano",
    latitude: 46.115665,
    longitude: 11.124966,
  },
  {
    station: "Mezzocorona Novali",
    latitude: 46.20977,
    longitude: 11.110052,
  },
  {
    station: "Mezzocorona Piovi Veci",
    latitude: 46.229237,
    longitude: 11.168802,
  },
  {
    station: "Mezzolombardo",
    latitude: 46.213787,
    longitude: 11.094399,
  },
  {
    station: "Mori",
    latitude: 45.847416,
    longitude: 10.992567,
  },
  {
    station: "Nago",
    latitude: 45.875774,
    longitude: 10.892236,
  },
  {
    station: "Nanno",
    latitude: 46.320042,
    longitude: 11.045521,
  },
  {
    station: "Nave San Rocco",
    latitude: 46.167896,
    longitude: 11.090272,
  },
  {
    station: "Nomi",
    latitude: 45.953716,
    longitude: 11.097726,
  },
  {
    station: "Ospedaletto",
    latitude: 46.034103,
    longitude: 11.572446,
  },
  {
    station: "Paneveggio",
    latitude: 46.309509,
    longitude: 11.747518,
  },
  {
    station: "Passo Vezzena",
    latitude: 45.96117,
    longitude: 11.31444,
  },
  {
    station: "Pedersano",
    latitude: 45.931816,
    longitude: 11.032673,
  },
  {
    station: "Pellizzano",
    latitude: 46.316032,
    longitude: 10.774681,
  },
  {
    station: "Pergine",
    latitude: 46.052563,
    longitude: 11.240219,
  },
  {
    station: "Pietramurata",
    latitude: 46.012108,
    longitude: 10.9536,
  },
  {
    station: "Pinzolo Pra Rodont",
    latitude: 46.166477,
    longitude: 10.785802,
  },
  {
    station: "Polsa",
    latitude: 45.781513,
    longitude: 10.947264,
  },
  {
    station: "Predazzo",
    latitude: 46.297848,
    longitude: 11.59826,
  },
  {
    station: "Pressano",
    latitude: 46.153046,
    longitude: 11.11015,
  },
  {
    station: "Rabbi",
    latitude: 46.410999,
    longitude: 10.806319,
  },
  {
    station: "Ravina Belvedere",
    latitude: 46.045898,
    longitude: 11.103955,
  },
  {
    station: "Revo",
    latitude: 46.38345,
    longitude: 11.057439,
  },
  {
    station: "Riva del Garda",
    latitude: 45.877514,
    longitude: 10.856446,
  },
  {
    station: "Romagnano",
    latitude: 46.011227,
    longitude: 11.115474,
  },
  {
    station: "Romeno",
    latitude: 46.394104,
    longitude: 11.109279,
  },
  {
    station: "Ronzo Chienis",
    latitude: 45.886196,
    longitude: 10.944168,
  },
  {
    station: "Rovere della Luna",
    latitude: 46.248653,
    longitude: 11.185551,
  },
  {
    station: "Rovereto",
    latitude: 45.87825,
    longitude: 11.0197,
  },
  {
    station: "S. Michele a/A",
    latitude: 46.183498,
    longitude: 11.12022,
  },
  {
    station: "Sarche",
    latitude: 46.03228,
    longitude: 10.956287,
  },
  {
    station: "Savignano",
    latitude: 45.942333,
    longitude: 11.05757,
  },
  {
    station: "S. Orsola",
    latitude: 46.09269,
    longitude: 11.285931,
  },
  {
    station: "Segno",
    latitude: 46.299171,
    longitude: 11.074924,
  },
  {
    station: "Serravalle",
    latitude: 45.805794,
    longitude: 11.01848,
  },
  {
    station: "Spormaggiore",
    latitude: 46.222683,
    longitude: 11.045454,
  },
  {
    station: "Sporminore",
    latitude: 46.238625,
    longitude: 11.023337,
  },
  {
    station: "Stenico",
    latitude: 46.050167,
    longitude: 10.863255,
  },
  {
    station: "Storo",
    latitude: 45.84676,
    longitude: 10.562442,
  },
  {
    station: "Telve",
    latitude: 46.058048,
    longitude: 11.47749,
  },
  {
    station: "Terlago",
    latitude: 46.087624,
    longitude: 11.046102,
  },
  {
    station: "Termon",
    latitude: 46.274612,
    longitude: 11.028917,
  },
  {
    station: "Terzolas",
    latitude: 46.358833,
    longitude: 10.923874,
  },
  {
    station: "Ton",
    latitude: 46.258751,
    longitude: 11.072985,
  },
  {
    station: "Toss - Castello",
    latitude: 46.2757,
    longitude: 11.085592,
  },
  {
    station: "Tovel",
    latitude: 46.257679,
    longitude: 10.944616,
  },
  {
    station: "Trento Sud",
    latitude: 46.024933,
    longitude: 11.126099,
  },
  {
    station: "Verla di Giovo",
    latitude: 46.154442,
    longitude: 11.15108,
  },
  {
    station: "Vigolo Vattaro",
    latitude: 46.154442,
    longitude: 11.194649,
  },
  {
    station: "Villazzano",
    latitude: 46.053867,
    longitude: 11.147566,
  },
  {
    station: "Volano",
    latitude: 45.92313,
    longitude: 11.05968,
  },
  {
    station: "Zambana",
    latitude: 46.156788,
    longitude: 11.070846,
  },
  {
    station: "Zortea",
    latitude: 46.165005,
    longitude: 11.757445,
  },
];

export { stations };
