// author: Alberto Scorrano
// email: alberto.scorrano@protonmail.ch
import "react-native-gesture-handler";
import { NavigationContainer } from "@react-navigation/native";
import React, { useEffect, useState } from "react";
import { Platform, AsyncStorage } from "react-native";
import { MenuProvider } from "react-native-popup-menu";
import Constants from "expo-constants";
import * as Notifications from "expo-notifications";
import * as Permissions from "expo-permissions";
import * as Font from "expo-font";
import AppLoading from "expo-app-loading";
import { AppContext } from "./src/contexts/appcontext";
import { MainNav } from "./src/Components/Navigations/Main/index";
import { TournamentNav } from "./src/Components/Navigations/Tournament/index";
import { TrentinoTournamentNav } from "./src/Components/Navigations/TrentinoTournament/index";
//console.disableYellowBox = true;
//LogBox.ignoreAllLogs(disable);

export default function App() {
  const [isReady, setIsReady] = useState(false);
  const [appMode, setAppMode] = useState("");
  const [token, setUniquenessToken] = useState<any>("");
  async function _setAssets() {
    const cachedFonts = await Font.loadAsync({
      Light: require("./assets/fonts/Quicksand-Light.ttf"),
      Regular: require("./assets/fonts/Quicksand-Regular.ttf"),
      Medium: require("./assets/fonts/Quicksand-Medium.ttf"),
      SB: require("./assets/fonts/Quicksand-SemiBold.ttf"),
      Bold: require("./assets/fonts/Quicksand-Bold.ttf"),
    });
    const images = [
      require("./assets/images/dati_trentino.png"),
      require("./assets/images/torneo_previsioni.png"),
      require("./assets/images/torneo_trentino.png"),
    ];
    // return Promise.all([cachedFonts, cachedImages]);
  }
  async function getExpoToken() {
    let token;
    if (Constants.isDevice) {
      const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
      let finalStatus = existingStatus;
      if (existingStatus !== "granted") {
        const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
        finalStatus = status;
      }
      if (finalStatus !== "granted") {
        alert("Failed to get push token for push notification!");
      }
      token = (await Notifications.getExpoPushTokenAsync()).data;
    } else {
      alert("Esegui l'app su un dispositivo fisico!");
    }

    if (Platform.OS === "android") {
      Notifications.setNotificationChannelAsync("default", {
        name: "default",
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 250, 250],
        lightColor: "#FF231F7C",
      });
    }
    await AsyncStorage.setItem("token", token);
    return token;
  }
  useEffect(() => {
    async function initialize() {
      const hasAlreadyToken = await AsyncStorage.getItem("token");
      const hasAppMode = await AsyncStorage.getItem("appmode");
      if (hasAppMode) setAppMode(hasAppMode);
      else setAppMode("");
      if (hasAlreadyToken !== null) {
        console.log("Abbiamo già un token!");
        console.log(hasAlreadyToken);
        setUniquenessToken(hasAlreadyToken);
      } else {
        console.log("Prima volta che impostiamo il token...");
        console.log(getExpoToken());
        setUniquenessToken(getExpoToken());
      }
    }
    initialize();
  });
  useEffect(() => {
    async function applyMode() {
      console.log("appmode: " + appMode);
      await AsyncStorage.setItem("appMode", appMode);
    }
    applyMode();
  }, [appMode]);
  return (
    <>
      {!isReady ? (
        <AppLoading
          startAsync={_setAssets}
          onError={console.warn}
          onFinish={() => {
            console.log("Ho finito!");
            setIsReady(true);
          }}
        />
      ) : (
        <NavigationContainer>
          <AppContext.Provider value={{ appMode, setAppMode }}>
            {appMode === "" && (
              <>
                {console.log("SDfljsdahn")}
                <MenuProvider>
                  <MainNav />
                </MenuProvider>
              </>
            )}
            {appMode === "trentino_tournament" && (
              <MenuProvider>
                <TrentinoTournamentNav />
              </MenuProvider>
            )}
            {appMode === "weather_tournament" && (
              <MenuProvider>
                <TournamentNav />
              </MenuProvider>
            )}
          </AppContext.Provider>
        </NavigationContainer>
      )}
    </>
  );
}
