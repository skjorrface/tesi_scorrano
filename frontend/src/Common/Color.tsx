export default {
  gray: "#808080",
  blue: "#388fff",
  white: "#FFFFFF",
  whiteSmoke: "#F2F2F3",
};
