export default {
  container: {
    flex: 1,
    backgroundColor: "#fff",
    marginHorizontal: 15,
    marginTop: 15,
  },

  textRegular9: {
    fontFamily: "Regular",
    fontSize: 9,
  },
  textLight9: {
    fontFamily: "Light",
    fontSize: 9,
  },
  textMedium9: {
    fontFamily: "Medium",
    fontSize: 9,
  },
  textSB9: {
    fontFamily: "SB",
    fontSize: 9,
  },
  textB9: {
    fontFamily: "Bold",
    fontSize: 9,
  },

  textRegular11: {
    fontFamily: "Regular",
    fontSize: 11,
  },
  textLight11: {
    fontFamily: "Light",
    fontSize: 11,
  },
  textMedium11: {
    fontFamily: "Medium",
    fontSize: 11,
  },
  textSB11: {
    fontFamily: "SB",
    fontSize: 11,
  },
  textB11: {
    fontFamily: "Bold",
    fontSize: 11,
  },

  textRegular12: {
    fontFamily: "Regular",
    fontSize: 12,
  },
  textLight12: {
    fontFamily: "Light",
    fontSize: 12,
  },
  textMedium12: {
    fontFamily: "Medium",
    fontSize: 12,
  },
  textSB12: {
    fontFamily: "SB",
    fontSize: 12,
  },
  textB12: {
    fontFamily: "Bold",
    fontSize: 12,
  },

  textRegular14: {
    fontFamily: "Regular",
    fontSize: 14,
  },
  textLight14: {
    fontFamily: "Light",
    fontSize: 14,
  },
  textMedium14: {
    fontFamily: "Medium",
    fontSize: 14,
  },
  textSB14: {
    fontFamily: "SB",
    fontSize: 14,
  },
  textB14: {
    fontFamily: "Bold",
    fontSize: 14,
  },

  textRegular16: {
    fontFamily: "Regular",
    fontSize: 16,
  },
  textLight16: {
    fontFamily: "Light",
    fontSize: 16,
  },
  textMedium16: {
    fontFamily: "Medium",
    fontSize: 16,
  },
  textSB16: {
    fontFamily: "SB",
    fontSize: 16,
  },
  textB16: {
    fontFamily: "Bold",
    fontSize: 16,
  },

  textB18: {
    fontFamily: "Bold",
    fontSize: 18,
  },

  textSB18: {
    fontFamily: "SB",
    fontSize: 18,
  },
  textSB20: {
    fontFamily: "SB",
    fontSize: 20,
  },
  textSB24: {
    fontFamily: "SB",
    fontSize: 24,
  },

  textBold18: {
    fontFamily: "Bold",
    fontSize: 18,
  },
  textBold20: {
    fontFamily: "Bold",
    fontSize: 20,
  },
  textBold24: {
    fontFamily: "Bold",
    fontSize: 24,
  },
  textBold28: {
    fontFamily: "SB",
    fontSize: 32,
  },
};
