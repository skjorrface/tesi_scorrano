import React from "react";
import { View, Text, Image } from "react-native";
import Modal from "react-native-modal";
import Color from "../../Common/Color";
import List from "../ImageText/index";

const info = require("../../../assets/images/info.png");
const temp = require("../../../assets/images/temp.png");
const vent = require("../../../assets/images/vent.png");
const pio = require("../../../assets/images/pio.png");
const umid = require("../../../assets/images/umid.png");

export default (props) => {
  let messageIcon;
  let messageColor;
  if (props.messageType === "error") {
    messageColor = Color.red;
    messageIcon = require("../../../assets/images/error.png");
  } else if (props.messageType === "info") {
    messageColor = "#000000";
    messageIcon = require("../../../assets/images/info.png");
  } else {
    messageColor = "#FFFFFF";
    messageIcon = require("../../../assets/images/error.png");
  }
  return (
    <Modal isVisible={props.visibility} onBackdropPress={() => props.toggleFunc(false)}>
      <View
        style={{
          flex: 1,
          height: 200,
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <View
          style={{
            flexDirection: "column",
            border: "2px solid",
            borderRadius: 10,
            backgroundColor: "rgba(32,32,32,1)",
            width: 300,
            height: 300,
          }}
        >
          <View
            style={{
              height: 30,
              backgroundColor: messageColor,
            }}
          >
            <List source={info} hasBorder={false} conText={props.timestamp + ", " + props.station} />
          </View>
          <View style={{ height: 300 }}>
            <List source={temp} conText={"Temp: " + props.dataset.temp} />
            <List source={pio} conText={"Pio: " + props.dataset.pio} />
            <List source={umid} conText={"Umid: " + props.dataset.umid} />
            <List source={vent} conText={"Vent: " + props.dataset.vent} />
          </View>
        </View>
      </View>
    </Modal>
  );
};
