import React from "react";
import Styles from "./style";
import { Text, View, TouchableOpacity } from "react-native";
import Spinner from "../Spinner/index";

const Button = (props: any) => {
  return (
    <TouchableOpacity onPress={props.onPress} style={{ marginTop: props.marginTop || 0, width: 220 }}>
      <View style={[Styles.viewStyle, props.viewStyle]}>
        <View>
          {!props.isDisabled && <Text style={[Styles.textStyle, props.textStyle]}>{props.Text}</Text>}
          {props.isDisabled && <Spinner />}
        </View>
      </View>
    </TouchableOpacity>
  );
};
export default Button;
