import Color from "../../Common/Color";
export default {
  viewStyle: {
    width: "60%",
    marginLeft: "20%",
    backgroundColor: Color.blue,
    borderRadius: 100,
    // borderColor: Color.black,
    justifyContent: "center",
    // margin: 10
  },
  textStyle: {
    fontSize: 15,
    textAlign: "center",
    color: Color.white,
    fontFamily: "SB",
    // marginHorizontal: 40,
    marginVertical: 15,
    //fontWeight: '800',
  },
};
