import React from "react";
import { Text, View } from "react-native";
import List from "../../Components/ImageText/index";
import Style from "../../Common/style";
const vent = require("../../../assets/images/vent.png");

export default (props) => {
  let item = props.item;
  if (props.item === undefined)
    item = {
      provider: "N/D",
      MinTemp: "N/D",
      MaxTemp: "N/D",
      Windspeed: "N/D",
      WindDirection: "N/D",
      RainProbability: "N/D",
      MmOfRain: "N/D",
      Humidity: "N/D",
      CloudCover: "N/D",
    };
  return (
    <View
      style={{
        flex: 1,
        height: 520,
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        marginTop: 3,
        marginBottom: 10,
      }}
    >
      <View
        style={{
          flexDirection: "column",
          borderRadius: 10,
          backgroundColor: "rgba(32,32,32,1)",
          width: 350,
          height: 520,
        }}
      >
        <View
          style={{
            height: 30,
            backgroundColor: "black",
          }}
        >
          <Text style={[Style.textRegular16, { color: "gray", textAlign: "center", marginTop: 1 }]}>
            {item.provider === "Darksky"
              ? item.type === "Groundtruth"
                ? "Darksky (Groundtruth)"
                : "Darksky (Previsione)"
              : item.provider}
          </Text>
        </View>
        <View style={{ height: 300 }}>
          <List source={vent} conText={"Temperatura Minima: " + item.MinTemp.toFixed(2)} />
          <List source={vent} conText={"Temperatura Massima: " + item.MaxTemp.toFixed(2)} />
          <List source={vent} conText={"Velocità del Vento: " + item.WindSpeed.toFixed(2)} />
          <List source={vent} conText={"Direzione del Vento: " + item.WindDirection} />
          <List source={vent} conText={"Probabilità Pioggia: " + item.RainProbability.toFixed(2)} />
          <List source={vent} conText={"Mm Pioggia: " + item.MmOfRain.toFixed(2)} />
          <List
            source={vent}
            conText={parseFloat(item.Humidity) !== 800.93 ? "Umidità: " + item.Humidity.toFixed(2) : "Umidità: N/A"}
          />
          {/* <List source={vent} conText={"Copertura Nuvole: " + item.CloudCover || "N/A"} /> */}
        </View>
      </View>
    </View>
  );
};
