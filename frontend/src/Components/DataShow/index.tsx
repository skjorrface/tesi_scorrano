import React from "react";
import { Text, View } from "react-native";
import List from "../../Components/ImageText/index";
import Style from "../../Common/style";
const temp = require("../../../assets/images/temp.png");
const vent = require("../../../assets/images/vent.png");
const pio = require("../../../assets/images/pio.png");
const umid = require("../../../assets/images/umid.png");

export default (props) => {
  return (
    <>
      <View
        style={{
          flex: 1,
          height: 300,
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          marginTop: 3,
          marginBottom: 10,
        }}
      >
        <View
          style={{
            flexDirection: "column",
            borderRadius: 10,
            backgroundColor: "rgba(32,32,32,1)",
            width: 300,
            height: 300,
          }}
        >
          <View
            style={{
              height: 30,
              backgroundColor: "black",
            }}
          >
            <Text style={[Style.textRegular16, { color: "gray", textAlign: "center", marginTop: 1 }]}>
              {props.item.Provider || "Fmach"}
            </Text>
          </View>
          <View style={{ height: 300 }}>
            <List source={temp} conText={"Temp: " + props.item.temp.toFixed(2) || "N/A"} />
            <List source={pio} conText={"Pio: " + props.item.pio.toFixed(2) || "N/A"} />
            <List source={umid} conText={"Umid: " + props.item.umid.toFixed(2) || "N/A"} />
            <List source={vent} conText={"Vent: " + props.item.vent.toFixed(2) || "N/A"} />
          </View>
        </View>
      </View>
    </>
  );
};
