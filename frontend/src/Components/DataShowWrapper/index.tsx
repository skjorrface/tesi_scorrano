import React from "react";
import { View, FlatList, Text } from "react-native";
import ScheduleItem from "../../Components/ScheduleItem/index";
import Style from "../../Common/style";
import dayjs from "dayjs";

export default (props: {
  locationName: string; // locationName.split(" ")[0]
  issueDate: string; // scheduleQuery.data?.issueDate
  navigation: any;
  routekey: number; // parseInt(sceneprops.route.key)
  tournamentDuration: string; // scheduleQuery.data?.tournamentDuration
  models: {}[];
}) => (
  <>
    <View style={{ flex: 1 }}>
      <FlatList
        ListHeaderComponent={() => (
          <View style={{ flex: 1, flexDirection: "column", marginTop: 40 }}>
            <View style={{ alignSelf: "flex-start" }}>
              <Text style={[Style.textBold24]}>Torneo: Bari</Text>
              {/* props.locationName.toUpperCase() */}
              <Text style={[Style.textBold20]}>Data inizio: {props.issueDate}</Text>
              <Text style={[Style.textBold20]}>Durata Torneo: {props.tournamentDuration} giorni</Text>
            </View>
            <View style={{ alignSelf: "flex-start", marginTop: 10 }}></View>
          </View>
        )}
        style={{ marginLeft: "7%" }}
        data={[
          { station: props.locationName, issueDate: props.issueDate, houroffset: 1 },
          { station: props.locationName, issueDate: props.issueDate, houroffset: 3 },
          { station: props.locationName, issueDate: props.issueDate, houroffset: 5 },
        ]}
        renderItem={({ item, index }) => {
          return (
            <>
              <ScheduleItem
                onPress={() =>
                  props.navigation.navigate("Tournament_Detail", {
                    item: props.models.filter((el: any) => {
                      if (
                        (el.dayDate ===
                          dayjs(props.issueDate)
                            .add(props.routekey - 1, "day")
                            .format("YYYY-MM-DD") &&
                          el.dayDistance === 2 * index + 1) ||
                        (el.dayDate ===
                          dayjs(props.issueDate)
                            .add(props.routekey - 1 + 2, "day")
                            .format("YYYY-MM-DD") &&
                          el.type === "Groundtruth")
                      )
                        return el;
                    }, props.issueDate),
                    tournamentDay: dayjs(props.issueDate).add(props.routekey, "day").format("YYYY-MM-DD"),
                  })
                }
                opacity={
                  dayjs().isAfter(dayjs(props.issueDate).add(item.houroffset, "hour").add(2, "minute")) ? 1 : 0.5
                } // QUI E' DA AGGIUSTARE!!!
                tournamentDay={props.routekey}
                tournamentType="classic"
                houroffset={item.houroffset}
                issueDate={props.issueDate}
                station="Bari"
              />
            </>
          );
        }}
        horizontal={false}
        numColumns={2}
        showsHorizontalScrollIndicator={false}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  </>
);
