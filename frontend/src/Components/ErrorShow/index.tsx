import React from "react";
import { Text, View, AsyncStorage } from "react-native";
import List from "../../Components/ImageText/index";
import { BarChart } from "react-native-chart-kit";
import { useQuery } from "react-query";
import axios from "axios";
import Constants from "../../Common/constant";
import Style from "../../Common/style";

const info = require("../../../assets/images/info.png");
const temp = require("../../../assets/images/temp.png");
const vent = require("../../../assets/images/vent.png");
const pio = require("../../../assets/images/pio.png");
const umid = require("../../../assets/images/umid.png");

interface Item {
  temp: number;
  vent: number;
  umid: number;
  pio: number;
  Provider: string;
}

interface ErrorShowProps {
  provider: "Darksky" | "Climacell" | "Accuweather" | "Openweathermap";
  houroffset: number;
  item: Item;
  gtruth: Item;
}

export default (props: ErrorShowProps) => {
  const rmses = useQuery(["trentinoRmses", props.provider], async () => {
    const tournamentIdentity = await AsyncStorage.getItem("token");
    return axios({
      url: Constants.url,
      method: "post",
      data: {
        query: `
            query {
              getTrentinoRmse(tournamentIdentity: "m4rr56-g", houroffset: ${props.houroffset}) {
                id
                houroffset
                Provider
                tempRmse
                umidRmse
                pioRmse
                ventRmse
                totalRmse
              }
            }
          `,
      },
    }).then((data) => {
      return data.data.data.getTrentinoRmse;
    });
  });
  const singleDataSet = rmses.data ? rmses.data.filter((el) => el.Provider === props.provider) : [];
  console.log("singleDataSet: " + JSON.stringify(singleDataSet));
  const data = {
    labels: ["tempE", "ventE", "pioE", "umidE"],
    datasets: [
      {
        data:
          singleDataSet.length != 0
            ? [
                singleDataSet[0].tempRmse,
                singleDataSet[0].ventRmse,
                singleDataSet[0].pioRmse,
                singleDataSet[0].umidRmse,
              ]
            : [0, 0, 0, 0],
      },
    ],
  };
  return (
    <View
      style={{
        flex: 1,
        height: 590,
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        marginTop: 3,
        marginBottom: 20,
      }}
    >
      <View
        style={{
          flexDirection: "column",
          borderRadius: 10,
          backgroundColor: "rgba(32,32,32,1)",
          width: 300,
          height: 590,
        }}
      >
        <View
          style={{
            height: 30,
            backgroundColor: "black",
          }}
        >
          <Text style={[Style.textRegular16, { color: "gray", textAlign: "center", marginTop: 1 }]}>
            {props.item.Provider}
          </Text>
        </View>
        {rmses.data && (
          <View style={{ height: 300 }}>
            <List
              source={temp}
              conText={
                rmses.data.length !== 0
                  ? "Temp Rmse: " + rmses.data.filter((el) => el.Provider === props.provider)[0].tempRmse.toFixed(4)
                  : "Temp Rmse: N/A"
              }
            />
            <List
              source={pio}
              conText={
                rmses.data.length !== 0
                  ? "Pio Rmse: " + rmses.data.filter((el) => el.Provider === props.provider)[0].pioRmse.toFixed(4)
                  : "Pio Rmse: N/A"
              }
            />
            <List
              source={umid}
              conText={
                rmses.data.length !== 0
                  ? "Umid Rmse: " + rmses.data.filter((el) => el.Provider === props.provider)[0].umidRmse.toFixed(4)
                  : "Temp Rmse: N/A"
              }
            />
            <List
              source={vent}
              conText={
                rmses.data.length !== 0
                  ? "Vent Rmse: " + rmses.data.filter((el) => el.Provider === props.provider)[0].ventRmse.toFixed(4)
                  : "Vent Rmse: N/A"
              }
            />
            <List
              source={info}
              conText={
                rmses.data.length !== 0
                  ? "Total Rmse: " + rmses.data.filter((el) => el.Provider === props.provider)[0].totalRmse.toFixed(4)
                  : "Total Rmse: N/A"
              }
            />
            <BarChart
              data={data}
              width={300}
              height={220}
              withCustomBarColorFromData={true}
              flatColor={true}
              chartConfig={{
                backgroundColor: "gray",
                backgroundGradientFrom: "#f0e3e3",
                backgroundGradientTo: "#afa9a9",
                data: data.datasets,
                color: () => "#161515",
                labelColor: () => "#6a6a6a",
              }}
            />
          </View>
        )}
      </View>
    </View>
  );
};
