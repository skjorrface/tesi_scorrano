import React from "react";
import { Text, View, Image, TouchableOpacity } from "react-native";
import Color from "../../Common/Color";
import style from "../../Common/style";
export default ({ navigation, source, onPress, hasBorder, conText }) => {
  return (
    <View
      style={{
        marginBottom: 8,
        marginHorizontal: 15,
        borderBottomColor: Color.gray,
        borderBottomWidth: hasBorder ? 3 : 0,
      }}
    >
      <TouchableOpacity style={{ flexDirection: "row", marginVertical: 15 }} onPress={onPress}>
        <View style={{ flex: 0.1, justifyContent: "center" }}>
          <Image style={{ width: 25, height: 18, resizeMode: "contain" }} source={source} />
        </View>
        <View style={{ flex: 0.8, justifyContent: "center" }}>
          <Text style={[style.textBold18, { color: Color.white, marginBottom: 4 }]}>{conText}</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};
