import React from "react";
import { View, Text } from "react-native";
import style from "./style";
import { Input } from "react-native-elements";
export default (props: any) => {
  const { icon, refInput, errorMessage, onSubmitEditing, marginTop, ...otherProps } = props;
  return (
    <View style={{ marginTop: marginTop || 0 }}>
      <Text style={style.text}>{props.placeholder}</Text>
      <Input
        {...otherProps}
        ref={refInput}
        // inputContainerStyle={style.inputContainer}
        inputStyle={style.textViewContainer}
        autoFocus={false}
        autoCapitalize="none"
        keyboardAppearance="dark"
        // errorStyle={style.errorInputStyle}
        autoCorrect={false}
        blurOnSubmit={false}
        placeholderTextColor="#A0A0A0"
        errorMessage={errorMessage}
      />
    </View>
  );
};
