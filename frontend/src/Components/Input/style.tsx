import Color from "../../Common/Color";
export default {
  textViewContainer: {
    height: 40,
    fontSize: 16,
    fontFamily: "Medium",
    // marginVertical: 15,
    borderBottomColor: "#DCE2EE",
    borderBottomWidth: 0,
    paddingVertical: 10,
  },
  text: {
    fontFamily: "SB",
    fontSize: 16,
    color: Color.gray,
    // marginTop: 10,
  },
};
