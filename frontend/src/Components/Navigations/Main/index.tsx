import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { MenuProvider } from "react-native-popup-menu";
import MainScreen from "../../../Screens/Main/index";
import TrentinoPickScreen from "../../../Screens/TrentinoPick/index";
import TrentinoSchedulesScreen from "../../../Screens/TrentinoSchedules/index";
import TrentinoTournamentConfirmScreen from "../../../Screens/TrentinoTournament/Confirm/index";
import TournamentPickScreen from "../../../Screens/Tournament/Pick/index";

export const MainNav: React.FC<{}> = () => {
  const Stack = createStackNavigator();
  return (
    <MenuProvider>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}
      >
        <Stack.Screen name="Home" component={MainScreen} />
        <Stack.Screen name="TrentinoPick" component={TrentinoPickScreen} />
        <Stack.Screen name="TournamentPick" component={TournamentPickScreen} />
        <Stack.Screen name="TrentinoSchedule" component={TrentinoSchedulesScreen} />
        <Stack.Screen name="TrentinoTournament_Confirm" component={TrentinoTournamentConfirmScreen} />
      </Stack.Navigator>
    </MenuProvider>
  );
};
