import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { MenuProvider } from "react-native-popup-menu";
import TournamentSummaryScreen from "../../../Screens/Tournament/Summary/index";
import TournamentDetailScreen from "../../../Screens/Tournament/Detail/index";
import { AsyncStorage } from "react-native";

export const TournamentNav: React.FC<{}> = () => {
  const Stack = createStackNavigator();
  return (
    <MenuProvider>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}
      >
        <Stack.Screen name="Tournament_Summary" component={TournamentSummaryScreen} />
        <Stack.Screen name="Tournament_Detail" component={TournamentDetailScreen} />
      </Stack.Navigator>
    </MenuProvider>
  );
};
