import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { MenuProvider } from "react-native-popup-menu";
import TrentinoTournamentSummaryScreen from "../../../Screens/TrentinoTournament/Summary/index";
import TrentinoTournamentDetailScreen from "../../../Screens/TrentinoTournament/Detail/index";

export const TrentinoTournamentNav: React.FC<{}> = () => {
  const Stack = createStackNavigator();
  return (
    <MenuProvider>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}
      >
        <Stack.Screen name="TrentinoTournament_Summary" component={TrentinoTournamentSummaryScreen} />
        <Stack.Screen name="TrentinoTournament_Detail" component={TrentinoTournamentDetailScreen} />
      </Stack.Navigator>
    </MenuProvider>
  );
};
