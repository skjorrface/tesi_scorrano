import React from "react";
import { View, Text } from "react-native";
import List from "../../../Components/ImageText/index";
import Style from "../../../Common/style";

interface RankItemProps {
  stat: string;
  data: { provider: string; value: number }[];
}

const rank_1 = require("../../../../assets/images/rank_1.png");
const rank_2 = require("../../../../assets/images/rank_2.png");
const rank_3 = require("../../../../assets/images/rank_3.png");
const rank_4 = require("../../../../assets/images/rank_4.png");

export default (props: RankItemProps) => {
  return (
    <View
      style={{
        flex: 1,
        height: 400,
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        marginTop: 3,
        marginBottom: 10,
      }}
    >
      <View
        style={{
          flexDirection: "column",
          borderRadius: 10,
          backgroundColor: "rgba(32,32,32,1)",
          width: 300,
          height: 400,
        }}
      >
        <View style={{ height: 30, backgroundColor: "black" }}>
          <Text style={[Style.textRegular16, { color: "gray", textAlign: "center", marginTop: 1 }]}>{props.stat}</Text>
        </View>
        <View style={{ height: 300 }}>
          {props.data
            .sort((firstEl, secondEl) => (firstEl.value < secondEl.value ? -1 : 1))
            .map((el, index) => (
              <List
                source={index === 0 ? rank_1 : index === 1 ? rank_2 : index === 2 ? rank_3 : rank_4}
                conText={el.provider + ": " + el.value.toFixed(4)}
              />
            ))}
        </View>
      </View>
    </View>
  );
};
