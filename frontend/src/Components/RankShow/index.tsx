import React, { useState, useEffect } from "react";
import Constants from "../../Common/constant";
import axios from "axios";
import { useQuery } from "react-query";
import RankItem from "./RankItem/index";
import { AsyncStorage } from "react-native";

export default () => {
  const [tempDarksky, setTempDarksky] = useState(0);
  const [tempAccuweather, setTempAccuweather] = useState(0);
  const [tempClimacell, setTempClimacell] = useState(0);
  const [tempOpenweathermap, setTempOpenweathermap] = useState(0);

  const [pioDarksky, setPioDarksky] = useState(0);
  const [pioAccuweather, setPioAccuweather] = useState(0);
  const [pioClimacell, setPioClimacell] = useState(0);
  const [pioOpenweathermap, setPioOpenweathermap] = useState(0);

  const [ventDarksky, setVentDarksky] = useState(0);
  const [ventAccuweather, setVentAccuweather] = useState(0);
  const [ventClimacell, setVentClimacell] = useState(0);
  const [ventOpenweathermap, setVentOpenweathermap] = useState(0);

  const [umidDarksky, setUmidDarksky] = useState(0);
  const [umidAccuweather, setUmidAccuweather] = useState(0);
  const [umidClimacell, setUmidClimacell] = useState(0);
  const [umidOpenweathermap, setUmidOpenweathermap] = useState(0);

  const [totalDarksky, setTotalDarksky] = useState(0);
  const [totalAccuweather, setTotalAccuweather] = useState(0);
  const [totalClimacell, setTotalClimacell] = useState(0);
  const [totalOpenweathermap, setTotalOpenweathermap] = useState(0);

  const rmses = useQuery(
    "trentinoRmses",
    async () => {
      const tournamentIdentity = await AsyncStorage.getItem("token");
      return axios({
        url: Constants.url,
        method: "post",
        data: {
          query: `
            query {
              getTrentinoRmseAll(tournamentIdentity: "m4rr56-g") {
                id
                Provider
                tempRmse
                umidRmse
                pioRmse
                ventRmse
                totalRmse
              }
            }
          `,
        },
      }).then((data) => data.data.data.getTrentinoRmseAll);
    },
    {
      onSuccess: () => {},
    }
  );
  useEffect(() => {
    setTempDarksky(0);
    setTotalDarksky(0);
    setPioDarksky(0);
    setVentDarksky(0);
    setUmidDarksky(0);
    setTempAccuweather(0);
    setTotalAccuweather(0);
    setPioAccuweather(0);
    setVentAccuweather(0);
    setUmidAccuweather(0);
    setTempClimacell(0);
    setTotalClimacell(0);
    setPioClimacell(0);
    setVentClimacell(0);
    setUmidClimacell(0);
    setTempOpenweathermap(0);
    setTotalOpenweathermap(0);
    setPioOpenweathermap(0);
    setVentOpenweathermap(0);
    setUmidOpenweathermap(0);
    const services = ["Darksky", "Accuweather", "Climacell", "Openweathermap"];
    for (let i = 0; i < services.length; i++) {
      if (rmses.data)
        rmses.data.forEach((el) => {
          if (el.Provider === services[i]) {
            switch (i) {
              case 0:
                setTempDarksky((prev) => prev + el.tempRmse);
                setPioDarksky((prev) => prev + el.pioRmse);
                setVentDarksky((prev) => prev + el.ventRmse);
                setUmidDarksky((prev) => prev + el.umidRmse);
                setTotalDarksky((prev) => prev + el.totalRmse);
                break;
              case 1:
                setTempAccuweather((prev) => prev + el.tempRmse);
                setPioAccuweather((prev) => prev + el.pioRmse);
                setVentAccuweather((prev) => prev + el.ventRmse);
                setUmidAccuweather((prev) => prev + el.umidRmse);
                setTotalAccuweather((prev) => prev + el.totalRmse);
                break;
              case 2:
                setTempClimacell((prev) => prev + el.tempRmse);
                setPioClimacell((prev) => prev + el.pioRmse);
                setVentClimacell((prev) => prev + el.ventRmse);
                setUmidClimacell((prev) => prev + el.umidRmse);
                setTotalClimacell((prev) => prev + el.totalRmse);
              case 3:
                setTempOpenweathermap((prev) => prev + el.tempRmse);
                setPioOpenweathermap((prev) => prev + el.pioRmse);
                setVentOpenweathermap((prev) => prev + el.ventRmse);
                setUmidOpenweathermap((prev) => prev + el.umidRmse);
                setTotalOpenweathermap((prev) => prev + el.totalRmse);
            }
          }
        });
    }
  }, [rmses.data]);
  return (
    <>
      <RankItem
        stat="Migliore in total"
        data={[
          {
            provider: "Darksky",
            value: totalDarksky,
          },
          {
            provider: "Climacell",
            value: totalClimacell,
          },
          {
            provider: "Accuweather",
            value: totalAccuweather,
          },
          {
            provider: "Openweathermap",
            value: totalOpenweathermap,
          },
        ]}
      />
      <RankItem
        stat="Migliore in temp"
        data={[
          {
            provider: "Darksky",
            value: tempDarksky,
          },
          {
            provider: "Climacell",
            value: tempClimacell,
          },
          {
            provider: "Accuweather",
            value: tempAccuweather,
          },
          {
            provider: "Openweathermap",
            value: tempOpenweathermap,
          },
        ]}
      />
      <RankItem
        stat="Migliore in vent"
        data={[
          {
            provider: "Darksky",
            value: ventDarksky,
          },
          {
            provider: "Climacell",
            value: ventClimacell,
          },
          {
            provider: "Accuweather",
            value: ventAccuweather,
          },
          {
            provider: "Openweathermap",
            value: ventOpenweathermap,
          },
        ]}
      />
      <RankItem
        stat="Migliore in pio"
        data={[
          {
            provider: "Darksky",
            value: pioDarksky,
          },
          {
            provider: "Climacell",
            value: pioClimacell,
          },
          {
            provider: "Accuweather",
            value: pioAccuweather,
          },
          {
            provider: "Openweathermap",
            value: pioOpenweathermap,
          },
        ]}
      />
      <RankItem
        stat="Migliore in umid"
        data={[
          {
            provider: "Darksky",
            value: umidDarksky,
          },
          {
            provider: "Climacell",
            value: umidClimacell,
          },
          {
            provider: "Accuweather",
            value: umidAccuweather,
          },
          {
            provider: "Openweathermap",
            value: umidOpenweathermap,
          },
        ]}
      />
    </>
  );
};
