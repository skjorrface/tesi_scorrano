import React from "react";
import { ScrollView } from "react-native";
import rmse from "rmse";
import dayjs from "dayjs";
import SingleParameter from "./singleparameter";

export default (props: {
  locationName: string; // locationName.split(" ")[0]
  issueDate: string; // scheduleQuery.data?.issueDate
  tournamentDuration: string; // scheduleQuery.data?.tournamentDuration
  data: {
    provider: string;
    type: string;
    dayDate: string;
    MinTemp: number;
    MaxTemp: number;
    MmOfRain: number;
    RainProbability: number;
  }[];
}) => {
  const providers = ["Darksky", "Accuweather", "Openweathermap", "Climacell"];
  const parameters = ["totalRmse", "MinTempRmse", "MaxTempRmse", "MmOfRainRmse", "RainProbabilityRmse"];
  const forecasts = new Array(providers.length).fill([]);
  const groundtruth = props.data.filter((forecast) => forecast.type === "Groundtruth");
  providers.forEach((el, index) => {
    forecasts[index] = props.data.filter((forecast) => forecast.provider === el && forecast.type === "Forecast");
  });
  const rmses = new Array(providers.length).fill([]).map(() => {
    return {
      totalRmse: 0,
      MinTempRmse: 0,
      MaxTempRmse: 0,
      MmOfRainRmse: 0,
      RainProbabilityRmse: 0,
      provider: "",
    };
  });
  forecasts.forEach((provider) => {
    provider.forEach(
      (forecast: {
        provider: string;
        dayDate: string;
        dayDistance: string;
        MinTemp: number;
        MaxTemp: number;
        MmOfRain: number;
        RainProbability: number;
      }) => {
        const specificGt = groundtruth.filter(
          (gt) =>
            gt.dayDate ===
            dayjs(forecast.dayDate)
              .add(parseInt(forecast.dayDistance) + 1, "day")
              .format("YYYY-MM-DD")
        )[0];
        if (specificGt !== undefined) {
          rmses[providers.indexOf(forecast.provider)].provider = forecast.provider;
          rmses[providers.indexOf(forecast.provider)].MinTempRmse += rmse.rmse([
            {
              actual: specificGt.MinTemp,
              predicted: forecast.MinTemp,
            },
          ]);
          rmses[providers.indexOf(forecast.provider)].MaxTempRmse += rmse.rmse([
            {
              actual: specificGt.MaxTemp,
              predicted: forecast.MaxTemp,
            },
          ]);
          rmses[providers.indexOf(forecast.provider)].MmOfRainRmse += rmse.rmse([
            {
              actual: specificGt.MmOfRain,
              predicted: forecast.MmOfRain,
            },
          ]);
          rmses[providers.indexOf(forecast.provider)].RainProbabilityRmse += rmse.rmse([
            {
              actual: specificGt.RainProbability,
              predicted: forecast.RainProbability,
            },
          ]);
          rmses[providers.indexOf(forecast.provider)].totalRmse = rmse.rmse([
            {
              actual: specificGt.MinTemp,
              predicted: forecast.MinTemp,
            },
            {
              actual: specificGt.MaxTemp,
              predicted: forecast.MaxTemp,
            },
            {
              actual: specificGt.MmOfRain,
              predicted: forecast.MmOfRain,
            },
            {
              actual: specificGt.RainProbability,
              predicted: forecast.RainProbability,
            },
          ]);
        }
      }
    );
  });
  const ranks = new Array();
  parameters.forEach((parameter) => {
    ranks.push(
      rmses.map((item) => {
        return {
          provider: item.provider,
          [parameter]: item[parameter],
        };
      })
    );
  });
  ranks.forEach((rank) => {
    parameters.forEach((parameter) => {
      if (parameter in rank[0])
        rank.sort((first, second) => {
          if (second[parameter] > first[parameter]) return -1;
          return 1;
        });
    });
  });

  return (
    <>
      {console.log(JSON.stringify(ranks))}
      <ScrollView style={{ marginTop: 50 }}>
        {ranks.map((rank) => {
          return <SingleParameter data={rank} />;
        })}
      </ScrollView>
    </>
  );
};
