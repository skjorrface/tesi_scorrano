import React from "react";
import List from "../../Components/ImageText/index";
import { View, Text } from "react-native";
import Style from "../../Common/style";

const rank_1 = require("../../../assets/images/rank_1.png");
const rank_2 = require("../../../assets/images/rank_2.png");
const rank_3 = require("../../../assets/images/rank_3.png");
const rank_4 = require("../../../assets/images/rank_4.png");
const rankImages = [rank_1, rank_2, rank_3, rank_4];

interface DataItem {
  provider: string;
  totalRmse?: number;
  MinTempRmse?: number;
  MaxTempRmse?: number;
  RainProbabilityRmse?: number;
  MmOfRainRmse?: number;
}

export default (props: { data: DataItem[] }) => {
  return (
    <View
      style={{
        flex: 1,
        height: 300,
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        marginTop: 3,
        marginBottom: 10,
      }}
    >
      <View
        style={{
          flexDirection: "column",
          borderRadius: 10,
          backgroundColor: "rgba(32,32,32,1)",
          width: 300,
          height: 300,
        }}
      >
        <View
          style={{
            height: 30,
            backgroundColor: "black",
          }}
        >
          <Text style={[Style.textRegular16, { color: "gray", textAlign: "center", marginTop: 1 }]}>
            {Object.keys(props.data[0])[1]}
          </Text>
        </View>
        <View style={{ height: 300 }}>
          {props.data.map((item: DataItem, index) => (
            <List source={rankImages[index]} conText={item.provider + ": " + Object.values(item)[1].toFixed(2)} />
          ))}
        </View>
      </View>
    </View>
  );
};
