const one = require(`../../../assets/images/trentino_data1.png`);
const two = require(`../../../assets/images/trentino_data2.png`);
const three = require(`../../../assets/images/trentino_data3.png`);
const four = require(`../../../assets/images/trentino_data4.png`);
const five = require(`../../../assets/images/trentino_data5.png`);
const six = require(`../../../assets/images/trentino_data6.png`);
const seven = require(`../../../assets/images/trentino_data7.png`);
const eight = require(`../../../assets/images/trentino_data8.png`);
const nine = require(`../../../assets/images/trentino_data9.png`);
const ten = require(`../../../assets/images/trentino_data10.png`);
const eleven = require(`../../../assets/images/trentino_data11.png`);
const twelve = require(`../../../assets/images/trentino_data12.png`);
const oneday = require(`../../../assets/images/1-day.png`);
const threeday = require(`../../../assets/images/3-day.png`);
const fiveday = require(`../../../assets/images/5-day.png`);

export const Miniatures = [one, two, three, four, five, six, seven, eight, nine, ten, eleven, twelve];
export const Days = [oneday, threeday, fiveday];
