import React from "react";
import { Text, View, Image, StyleSheet, TouchableOpacity } from "react-native";
import dayjs from "dayjs";
import Color from "../../Common/Color";
import style from "../../Common/style";
import { Miniatures, Days } from "./images";

export default (props: any) => {
  return (
    <TouchableOpacity
      onPress={props.onPress}
      style={{
        backgroundColor: Color.white,
        width: 155,
        height: 180,
        marginRight: 20,
        marginVertical: 25,
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 6.27,
        elevation: 5,
        borderRadius: 6,
      }}
    >
      <View style={{ borderRadius: 6, overflow: "hidden", opacity: props.opacity }}>
        <Image
          style={[styles.imageThumbnail]}
          source={
            props.tournamentType !== "classic"
              ? Miniatures[props.houroffset - 1]
              : Days[props.houroffset === 1 ? 0 : props.houroffset === 3 ? 1 : 2]
          }
        />
        <View style={{ marginHorizontal: 10, marginVertical: 2 }}>
          <Text style={[style.textB14]}>Comune: {props.station.toUpperCase()}</Text>
          <Text style={[style.textMedium12, { color: Color.gray, textAlign: "center" }]}>
            {props.tournamentType === "classic"
              ? "Previsione a " +
                props.houroffset +
                " giorno/i, per il " +
                dayjs(props.issueDate)
                  .add(props.houroffset + props.tournamentDay - 1, "day")
                  .format("MM/DD")
              : "Orario: " + dayjs(props.issueDate).add(props.houroffset, "hour").format("HH:mm")}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  MainContainer: {
    justifyContent: "center",
    flex: 1,
    paddingTop: 30,
  },
  imageThumbnail: {
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 30,
    marginTop: 5,
    height: 100,
    width: 100,
  },
});
