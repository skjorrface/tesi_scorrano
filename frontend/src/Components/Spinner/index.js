import React from "react";
import Color from "../../Common/Color";
import WaveIndicator, { UIActivityIndicator } from "react-native-indicators";

export default () => {
  return (
    <>
      <UIActivityIndicator style={{ backgroundColor: Color.whiteSmoke }} color={Color.blue} />
    </>
  );
};
