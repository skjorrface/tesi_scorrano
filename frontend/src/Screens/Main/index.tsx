import React from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";
const DATI_TRENTINO = require("../../../assets/images/dati_trentino.png");
const TORNEO_TRENTINO = require("../../../assets/images/torneo_trentino.png");
const TORNEO_PREVISIONI = require("../../../assets/images/torneo_previsioni.png");
import Style from "../../Common/style";
import Color from "../../Common/Color";
import Button from "../../Components/Button/index";
import axios from "axios";
import { useQuery, useMutation, queryCache } from "react-query";
import Constants from "../../Common/constant";

export default ({ navigation }: { navigation: any }) => {
  const [daemonToggle] = useMutation(
    async () => {
      return axios({
        url: Constants.url,
        method: "post",
        data: {
          query: `
          mutation {
            daemonToggle
          }
        `,
        },
      });
    },
    {
      onSuccess: () => {
        queryCache.invalidateQueries("daemonStatus");
      },
      onError: (error) => {
        console.log("Abbiamo un errore: " + error);
      },
    }
  );
  const daemonStatus = useQuery(
    "daemonStatus",
    async () => {
      return axios({
        url: Constants.url,
        method: "post",
        data: {
          query: `
          query {
            daemonStatus
          }
        `,
        },
      }).then((data) => {
        return data.data.data.daemonStatus;
      });
    },
    {
      refetchInterval: 60000,
      onError: (error) => {
        console.log("Abbiamo un errore: " + error);
      },
    }
  );
  return (
    <>
      {daemonStatus.isError && <Text>Error!</Text>}
      {daemonStatus.isLoading && <Text>Loading...</Text>}
      {!daemonStatus.isError && (
        <View style={{ flex: 1 }}>
          <View style={{ flexDirection: "row", marginLeft: "5%", marginTop: "5%" }}>
            <Text style={[Style.textMedium16, { color: Color.gray, fontSize: 25 }]}>
              Benvenuto su WeatherTournament.
            </Text>
          </View>
          <View style={{ flexDirection: "row", marginLeft: "8%", marginTop: "5%" }}>
            <Text style={[Style.textMedium16, { color: Color.gray, fontSize: 15 }]}>
              Seleziona una modalità per iniziare.
            </Text>
          </View>
          <View style={{ flexDirection: "row", marginTop: 0, marginLeft: 0 }}></View>
          <View style={{ flexDirection: "row", marginTop: "10%" }}>
            <View>
              <TouchableOpacity onPress={() => navigation.navigate("TrentinoPick", { whatFor: "trentino_data" })}>
                <Image source={DATI_TRENTINO} />
              </TouchableOpacity>
            </View>
            <View style={{ alignItems: "center", justifyContent: "center", position: "absolute", right: 0 }}>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate("TrentinoPick", { whatFor: "trentino_tournament" });
                }}
              >
                <Image source={TORNEO_TRENTINO} />
              </TouchableOpacity>
            </View>
          </View>
          <View style={{ flexDirection: "row" }}>
            <View style={{ marginLeft: 80 }}>
              <TouchableOpacity onPress={() => navigation.navigate("TournamentPick")}>
                <Image source={TORNEO_PREVISIONI} />
              </TouchableOpacity>
            </View>
          </View>
          <View style={{ flexDirection: "row" }}>
            <View style={{ marginLeft: 60, marginTop: 40 }}>
              <Text style={[Style.textMedium16, { color: Color.gray, fontSize: 15 }]}>
                Demone fetch dei dati: {daemonStatus.data}
                {daemonStatus.data ? "Attivo." : "Disattivato."}
              </Text>
              <Button Text={daemonStatus.data ? "DISATTIVA" : "ATTIVA"} onPress={daemonToggle} marginTop={20} />
            </View>
          </View>
        </View>
      )}
    </>
  );
};
