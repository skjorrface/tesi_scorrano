import React from "react";
import { ScrollView, Text } from "react-native";
import DataShow from "../../../Components/DataShow/classic";
import Style from "../../../Common/style";

export default ({ route }) => {
  return (
    <>
      <ScrollView style={{ marginTop: 30 }}>
        <Text style={[Style.textBold24, { marginLeft: 15 }]}>Data Previsione: {route.params.tournamentDay}</Text>
        {route.params.item?.map((item: {}) => {
          return <DataShow item={item} />;
        })}
      </ScrollView>
    </>
  );
};
