import React from "react";
import { View, Text, AsyncStorage } from "react-native";
import dayjs from "dayjs";
import TextInput from "../../../Components/TextInput/index";
import Button from "../../../Components/Button/index";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Style from "../../../Common/style";
import { useMutation, useQuery } from "react-query";
import axios from "axios";
import Constants from "../../../Common/constant";
import { AppContext } from "../../../contexts/appcontext";
import DropDownPicker from "react-native-dropdown-picker";
import Color from "../../../Common/Color";

const TournamentPickScreen: React.FC<{ navigation: any }> = () => {
  const [createTournamentMutation] = useMutation(
    async () => {
      const token = await AsyncStorage.getItem("token");
      console.log(
        "Token: " +
          token +
          " latitude: " +
          latitude +
          " longitude: " +
          longitude +
          " tournamentDuration: " +
          tournamentDuration.duration
      );
      return await axios({
        url: Constants.url,
        method: "post",
        data: {
          query: `
          mutation {
            createTournament(
              issuerIdentity: "${token}",
              latitude: ${parseFloat(latitude)},
              longitude: ${parseFloat(longitude)},
              tournamentDuration: ${tournamentDuration.duration}
              
            ) {
              id
              tournamentDuration
              issueDate
            }
          }
        `,
        },
      });
    },
    {
      onSuccess: async (data) => {
        console.log(JSON.stringify(data.data));
        await AsyncStorage.setItem("issueDate", dayjs().format(data.data.issueDate));
        await AsyncStorage.setItem("tournamentDuration", String(tournamentDuration.duration));
        await AsyncStorage.setItem("locationName", getCoords.data.display_name);
        await AsyncStorage.setItem("appmode", "weather_tournament");
        setAppMode("weather_tournament");
      },
      onError: (data) => console.log(data),
    }
  );
  const { setAppMode } = React.useContext(AppContext);
  const [location, setLocation] = React.useState<string>("");
  const [latitude, setLatitude] = React.useState<string>("");
  const [locationValid, setLocationValid] = React.useState(true);
  const [longitude, setLongitude] = React.useState<string>("");
  const [locationSend, setLocationSend] = React.useState<string>("");
  const [tournamentDuration, setTournamentDuration] = React.useState<{ duration: number }>({ duration: 3 });
  const locationInput = React.useRef(null);
  const getCoords = useQuery(
    location,
    async () => {
      return axios({
        url: Constants.url,
        method: "post",
        data: {
          query: `
          query {
            getCoords(locationName: "${locationSend}") {
              display_name
              lon
              lat
            }
          }
        `,
        },
      }).then((data) => {
        console.log(data.data.data.getCoords);
        return data.data.data.getCoords;
      });
    },
    {
      enabled: locationSend,
    }
  );
  return (
    <>
      <KeyboardAwareScrollView keyboardShouldPersistTaps="handled" contentContainerStyle={Style.container}>
        <View style={[Style.container, { marginTop: "70%" }]}>
          <TextInput
            defaultValue={location}
            placeholder="Località"
            onChangeText={(location: string) => {
              setLocation(location);
            }}
            refInput={locationInput}
            errorMessage={locationValid ? null : "Inserisci una località valida!"}
            onSubmitEditing={() => {
              locationInput.current.focus();
            }}
          />
          <Text style={[Style.textMedium16, { color: Color.gray }]}>Durata del Torneo in giorni: </Text>
          <View style={{ marginBottom: 20 }}>
            <DropDownPicker
              items={[
                { label: "3", value: 3, icon: (data: number) => <Text>{data}</Text> },
                { label: "4", value: 4, icon: (data: number) => <Text>{data}</Text> },
                { label: "5", value: 5, icon: (data: number) => <Text>{data}</Text> },
              ]}
              defaultValue={tournamentDuration.duration}
              placeholder="Durata del torneo in giorni"
              containerStyle={{ height: 40 }}
              style={{ backgroundColor: "#fafafa" }}
              itemStyle={{
                justifyContent: "flex-start",
              }}
              dropDownStyle={{ backgroundColor: "#fafafa" }}
              onChangeItem={(item) =>
                setTournamentDuration({
                  duration: item.value,
                })
              }
            />
          </View>
          <View style={{ marginLeft: "15%" }}>
            <Button
              Text="CERCA"
              onPress={() => {
                setLocationSend(location);
              }}
            />
          </View>

          {getCoords.isLoading && <Text>Caricando...</Text>}
          {getCoords.data && (
            <View>
              <Text style={[Style.textB11, { textAlign: "center", marginTop: 5, marginBottom: 5 }]}>
                Località: {getCoords.data.length != 0 ? getCoords.data.display_name : ""}
              </Text>
              <View style={{ marginLeft: "15%" }}>
                <Button
                  Text="CREA TORNEO"
                  onPress={async () => {
                    setLatitude(getCoords.data.lat);
                    setLongitude(getCoords.data.lon);
                    createTournamentMutation();
                    //navigation.navigate("Tournament_Summary");
                  }}
                />
              </View>
            </View>
          )}
        </View>
      </KeyboardAwareScrollView>
    </>
  );
};

export default TournamentPickScreen;
