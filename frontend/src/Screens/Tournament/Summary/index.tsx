import React from "react";
import { AsyncStorage, Dimensions } from "react-native";
import { useQuery } from "react-query";
import Constants from "../../../Common/constant";
import axios from "axios";
import dayjs from "dayjs";
import { TabView, TabBar } from "react-native-tab-view";
import DataShowWrapper from "../../../Components/DataShowWrapper/index";
import RankShowWrapper from "../../../Components/RankShowWrapper/index";

const Tournament_Summary = ({ navigation }: any) => {
  const [locationName, setLocationName] = React.useState("");
  const [tournamentDuration, setTournamentDuration] = React.useState(0);
  const [issueDate, setIssueDate] = React.useState("");
  const [routes, setRoutes] = React.useState([]);
  const [index, setIndex] = React.useState(0);
  const scheduleQuery = useQuery("scheduleQuery", async () => {
    const token = await AsyncStorage.getItem("token");
    return axios({
      url: Constants.url,
      method: "post",
      data: {
        query: `
            query {
              getTournament(issuerIdentity: "${token}") {
                issueDate
                tournamentDuration
                models {
                  dayDate
                  type
                  provider
                  dayDistance
                  id
                  MinTemp
                  MaxTemp
                  Humidity
                  MmOfRain
                  RainProbability
                  WindSpeed
                  WindDirection
                }
              }
            }
          `,
      },
    }).then((data) => data.data.data.getTournament);
  });
  React.useEffect(() => {
    if ((tournamentDuration === 3 || tournamentDuration === 4 || tournamentDuration === 5) && issueDate != "") {
      setRoutes([]);
      setRoutes(
        new Array(tournamentDuration + 1).fill({ key: String(Math.random()), title: "#" }).map((item, index) => {
          if (index === tournamentDuration) return { key: "RISULTATI", title: "RISULTATI" };
          else return { key: String(++index), title: `# ${index++}` };
        })
      );
      setTournamentDays({});
      for (let i = 0; i < tournamentDuration; i++) {
        setTournamentDays({
          dayDistance: 1,
          dayDate: dayjs(issueDate)
            .add(i + 1, "day")
            .format("YYYY-MM-DD"),
        });
        setTournamentDays({
          dayDistance: 3,
          dayDate: dayjs(issueDate)
            .add(i + 1, "day")
            .format("YYYY-MM-DD"),
        });
        setTournamentDays({
          dayDistance: 5,
          dayDate: dayjs(issueDate)
            .add(i + 1, "day")
            .format("YYYY-MM-DD"),
        });
      }
    }
  }, [tournamentDuration, issueDate]);
  React.useEffect(() => {
    async function getLocationName() {
      const locationName = await AsyncStorage.getItem("locationName");
      const tournamentDuration = await AsyncStorage.getItem("tournamentDuration");
      const issueDate = await AsyncStorage.getItem("issueDate");
      setLocationName(locationName);
      setTournamentDuration(parseInt(tournamentDuration));
      setIssueDate(issueDate);
    }
    getLocationName();
  }, []);
  const initialLayout = { width: Dimensions.get("window").width };

  const renderTabBar = (props: any) => <TabBar {...props} style={{ backgroundColor: "black" }} />;
  const renderScene = (sceneprops: { route: { key: string } }) => {
    switch (sceneprops.route.key) {
      case "RISULTATI":
        return (
          <RankShowWrapper
            key={Math.random()}
            locationName={locationName.split(" ")[0]}
            issueDate={scheduleQuery.data?.issueDate}
            tournamentDuration={scheduleQuery.data?.tournamentDuration}
            data={scheduleQuery.data?.models || [{}]}
          />
        );
      default:
        return (
          <DataShowWrapper
            key={Math.random()}
            navigation={navigation}
            locationName={locationName.split(" ")[0]}
            issueDate={scheduleQuery.data?.issueDate}
            routekey={parseInt(sceneprops.route.key)}
            tournamentDuration={scheduleQuery.data?.tournamentDuration}
            models={scheduleQuery.data?.models}
          />
        );
    }
  };
  return (
    <>
      <TabView
        renderTabBar={renderTabBar}
        navigationState={{ index, routes }}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={initialLayout}
        style={{ marginTop: 25, backgroundColor: "white" }}
      />
    </>
  );
};

export default Tournament_Summary;
