import React from "react";
import { ScrollView, View, Text, TouchableOpacity } from "react-native";
import { TableView, Section, Cell } from "react-native-tableview-simple";
import axios from "axios";
import { useQuery } from "react-query";
import Style from "../../Common/style";
import Constants from "../../Common/constant";

export default ({ navigation, route }) => {
  const TrentinoStation = useQuery("trentinoStations", async () => {
    return axios({
      url: Constants.url,
      method: "POST",
      data: {
        query: `
          query {
            getTrentinoStation {
              id
              station
            }
          }
        `,
      },
    }).then((data) => {
      return data.data.data.getTrentinoStation;
    });
  });
  return (
    <>
      <ScrollView>
        <TableView style={{ flex: 1, marginTop: 10 }}>
          <Section hideSurroundingSeparators={true} roundedCorners={true} header="Seleziona uuuun comune del Trentino:">
            {TrentinoStation.data &&
              TrentinoStation.data.map((station: string) => (
                <Cell
                  hideSeparator={true}
                  cellContentView={
                    <TouchableOpacity
                      onPress={() => {
                        const whereTo =
                          route.params.whatFor === "trentino_data" ? "TrentinoSchedule" : "TrentinoTournament_Confirm";
                        navigation.navigate(whereTo, { station: station.station });
                      }}
                      style={{
                        flex: 1,
                        flexDirection: "row",
                        backgroundColor: "white",
                        marginTop: 2,
                        marginBottom: 2,
                        borderBottomWidth: 1,
                        borderBottomColor: "gray",
                        alignItems: "center",
                      }}
                    >
                      <View>
                        <Text style={[Style.textMedium16, { color: "gray", fontSize: 20, paddingBottom: 4 }]}>
                          {station.station.toUpperCase()}
                        </Text>
                      </View>
                    </TouchableOpacity>
                  }
                  key={station.station}
                  cellStyle="Subtitle"
                />
              ))}
          </Section>
        </TableView>
      </ScrollView>
    </>
  );
};
