import React from "react";
import { useQuery, useMutation, queryCache } from "react-query";
import { ScrollView, TouchableOpacity, View, Text } from "react-native";
import { TableView, Section, Cell } from "react-native-tableview-simple";
import axios from "axios";
import Alert from "../../Components/Alert/index";
import { Menu, MenuOptions, MenuOption, MenuTrigger } from "react-native-popup-menu";
import Style from "../../Common/style";
import Constants from "../../Common/constant";

export default ({ navigation, route }) => {
  const [showData, setShowData] = React.useState(false);
  const [timestamp, setTimestamp] = React.useState(false);
  const [dataSet, setDataSet] = React.useState({});
  const [deleteRecord, deleteRecordInfo] = useMutation(
    async (id: number) => {
      return axios({
        url: Constants.url,
        method: "post",
        data: {
          query: `
            mutation {
              deleteTrentinoSchedule(id:${id}) {
                id
              }
            }
          `,
        },
      });
    },
    {
      onSuccess: () => {
        queryCache.invalidateQueries("trentinoSchedules");
      },
    }
  );
  const schedules = useQuery("trentinoSchedules", async () => {
    return axios({
      url: Constants.url,
      method: "post",
      data: {
        query: `
          query {
            getTrentinoSchedules(station: "${route.params.station.toLowerCase()}") {
              id
              timestamp
              models {
                id
                vent
                pio
                umid
                temp
              }
            }
          }
        `,
      },
    }).then((data) => {
      return data.data.data.getTrentinoSchedules;
    });
  });
  return (
    <>
      <Alert
        visibility={showData}
        messageType="info"
        toggleFunc={setShowData}
        timestamp={timestamp}
        dataset={dataSet}
        station={route.params.station.toUpperCase()}
      />
      <ScrollView>
        <TableView style={{ flex: 1 }}>
          <Section
            hideSurroundingSeparators={true}
            roundedCorners={true}
            header={"Seleziona un record disponibile per " + route.params.station}
          >
            {schedules.data &&
              schedules.data.map((schedule) => (
                <Cell
                  hideSeparator={true}
                  cellContentView={
                    <TouchableOpacity
                      onPress={() => {}}
                      style={{
                        flex: 1,
                        flexDirection: "row",
                        backgroundColor: "white",
                        marginTop: 2,
                        marginBottom: 2,
                        borderBottomWidth: 1,
                        borderBottomColor: "gray",
                        alignItems: "center",
                      }}
                    >
                      <View>
                        <Menu key={schedule.id}>
                          <MenuTrigger
                            text={"timestamp: " + schedule.timestamp}
                            customStyles={{ triggerText: [Style.textMedium12, { fontSize: 15 }] }}
                          />
                          <MenuOptions>
                            <MenuOption
                              onSelect={() => {
                                setShowData(true);
                                setTimestamp(schedule.timestamp);
                                setDataSet(schedule.models[0]);
                              }}
                              text="Mostra Dati"
                            />
                            <MenuOption
                              onSelect={() => {
                                deleteRecord(schedule.id);
                              }}
                            >
                              <Text style={{ color: "red" }}>Cancella Record</Text>
                            </MenuOption>
                          </MenuOptions>
                        </Menu>
                      </View>
                    </TouchableOpacity>
                  }
                  key={schedule.id}
                  cellStyle="Subtitle"
                />
              ))}
          </Section>
        </TableView>
      </ScrollView>
    </>
  );
};
