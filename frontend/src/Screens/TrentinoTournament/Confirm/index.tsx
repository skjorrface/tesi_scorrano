import React from "react";
import { Text, View, AsyncStorage } from "react-native";
import Style from "../../../Common/style";
import Button from "../../../Components/Button/index";
import axios from "axios";
import { useMutation } from "react-query";
import { AppContext } from "../../../contexts/appcontext";
import dayjs from "dayjs";
import Constants from "../../../Common/constant";

export default ({ route, navigation }) => {
  const { setAppMode } = React.useContext(AppContext);
  const [createTrentinoTournamentMutation] = useMutation(
    async () => {
      const token = await AsyncStorage.getItem("token");
      return axios({
        url: Constants.url,
        method: "post",
        data: {
          query: `
          mutation {
            createTrentinoTournament(station:"${route.params.station.toLowerCase()}", issuerIdentity:"${token}") {
              id
            }
          }
        `,
        },
      });
    },
    {
      onSuccess: async () => {
        AsyncStorage.setItem("station", route.params.station.toLowerCase());
        AsyncStorage.setItem("issueDate", dayjs().format("YYYY-MM-DD HH:mm"));
        AsyncStorage.setItem("appmode", "trentino_tournament");
        setAppMode("trentino_tournament");
      },
    }
  );

  return (
    <>
      <Text style={[Style.textMedium16, { marginTop: "80%", marginBottom: 50, textAlign: "center" }]}>
        Creare torneo di 12 ore per la località {route.params.station.toUpperCase()}?
      </Text>
      <View style={{ flex: 1, flexDirection: "row" }}>
        <View style={{ marginLeft: -20 }}>
          <Button Text="Conferma" onPress={createTrentinoTournamentMutation} />
        </View>
        <View>
          <Button Text="Annulla" onPress={() => navigation.goBack()} />
        </View>
      </View>
    </>
  );
};
