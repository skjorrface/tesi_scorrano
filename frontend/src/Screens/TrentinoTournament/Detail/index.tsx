import React from "react";
import { ScrollView, StyleSheet, Dimensions } from "react-native";
import { TabView, TabBar } from "react-native-tab-view";
import Color from "../../../Common/Color";

import RankShow from "../../../Components/RankShow/index";
import ErrorShow from "../../../Components/ErrorShow/index";
import DataShow from "../../../Components/DataShow/index";

const initialLayout = { width: Dimensions.get("window").width };

const renderTabBar = (props: any) => (
  <TabBar {...props} indicatorStyle={{ backgroundColor: "white" }} style={{ backgroundColor: "black", fontSize: 8 }} />
);

export default function TabViewExample(props: any) {
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: "DATI", title: "DATI" },
    { key: "ERRORI", title: "ERRORI" },
    { key: "CLASSIFICA", title: "CLASSIFICA" },
  ]);
  const renderScene = (sceneprops: { route: { key: string } }) => {
    switch (sceneprops.route.key) {
      case "DATI":
        return (
          <ScrollView style={{ backgroundColor: Color.whiteSmoke }}>
            <DataShow
              item={
                props.route.params.fmach
                  ? props.route.params.fmach
                  : { vent: "N/A", pio: "N/A", temp: "N/A", umid: "N/A" }
              }
            />
            <DataShow item={props.route.params.darksky} />
            <DataShow item={props.route.params.climacell} />
            <DataShow item={props.route.params.accuweather} />
            <DataShow item={props.route.params.openweathermap} />
          </ScrollView>
        );
      case "ERRORI":
        return (
          <ScrollView style={{ backgroundColor: Color.whiteSmoke }}>
            <ErrorShow
              houroffset={props.route.params.houroffset}
              item={props.route.params.darksky}
              provider="Darksky"
              gtruth={props.route.params.fmach || { vent: 0, pio: 0, temp: 0, umid: 0 }}
            />
            <ErrorShow
              provider="Climacell"
              houroffset={props.route.params.houroffset}
              item={props.route.params.climacell}
              gtruth={props.route.params.fmach || { vent: 0, pio: 0, temp: 0, umid: 0 }}
            />
            <ErrorShow
              provider="Accuweather"
              houroffset={props.route.params.houroffset}
              item={props.route.params.accuweather}
              gtruth={props.route.params.fmach || { vent: 0, pio: 0, temp: 0, umid: 0 }}
            />
            <ErrorShow
              provider="Openweathermap"
              houroffset={props.route.params.houroffset}
              item={props.route.params.openweathermap}
              gtruth={props.route.params.fmach || { vent: 0, pio: 0, temp: 0, umid: 0 }}
            />
          </ScrollView>
        );
      case "CLASSIFICA":
        return (
          <ScrollView style={{ backgroundColor: Color.whiteSmoke }}>
            <RankShow />
          </ScrollView>
        );
    }
  };
  return (
    <>
      <TabView
        renderTabBar={renderTabBar}
        navigationState={{ index, routes }}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={initialLayout}
        style={{ marginTop: 30, backgroundColor: "black" }}
      />
    </>
  );
}
