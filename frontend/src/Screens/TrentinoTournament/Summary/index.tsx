import React, { useEffect } from "react";
import { View, Text, FlatList, AsyncStorage } from "react-native";
import ScheduleItem from "../../../Components/ScheduleItem/index";
import Style from "../../../Common/style";
import Color from "../../../Common/Color";
import { useQuery } from "react-query";
import axios from "axios";
import dayjs from "dayjs";
import Constants from "../../../Common/constant";

export default ({ route, navigation }) => {
  const [station, setStation] = React.useState("");
  const [issueDate, setIssueDate] = React.useState("");
  const trentinoScheduleQuery = useQuery(
    "trentinoSchedule",
    async () => {
      const token = await AsyncStorage.getItem("token");
      return axios({
        url: Constants.url,
        method: "post",
        data: {
          query: `
          query {
            getTrentinoData(tournamentIdentity:"${token}") {
              id
              houroffset
              models {
                id
                station
                Provider
                pio
                umid
                vent
                temp
              }
            }
          }
        `,
        },
      }).then((data) => {
        return data.data.data.getTrentinoData;
      });
    },
    {
      refetchInterval: 60000,
      onSuccess: () => {
        console.log("Refetched!");
      },
    }
  );
  useEffect(() => {
    async function getTournamentData() {
      const station = await AsyncStorage.getItem("station");
      const issueDate = await AsyncStorage.getItem("issueDate");
      setStation(station);
      setIssueDate(issueDate);
    }
    getTournamentData();
  }, [station, issueDate]);
  return (
    <>
      <View style={{ flex: 1 }}>
        <FlatList
          ListHeaderComponent={() => (
            <View style={{ flex: 1, flexDirection: "column", marginTop: 40 }}>
              <View style={{ alignSelf: "flex-start" }}>
                <Text style={[Style.textBold24]}>Torneo: {station}</Text>
                <Text style={[Style.textBold20]}>Data inizio: {issueDate}</Text>
              </View>
              <View style={{ alignSelf: "flex-start", marginTop: 10 }}></View>
            </View>
          )}
          style={{ marginLeft: "7%" }}
          data={trentinoScheduleQuery.data ? trentinoScheduleQuery.data : []}
          renderItem={({ item }) => {
            return (
              <>
                <ScheduleItem
                  opacity={dayjs().isAfter(dayjs(issueDate).add(item.houroffset, "hour").add(2, "minute")) ? 1 : 0.5}
                  houroffset={item.houroffset}
                  issueDate={issueDate}
                  station={station}
                  onPress={() => {
                    navigation.navigate("TrentinoTournament_Detail", {
                      accuweather: item.models.filter((el) => el.Provider === "Accuweather")[0],
                      darksky: item.models.filter((el) => el.Provider === "Darksky")[0],
                      fmach: item.models.filter((el) => el.Provider === "Fmach")[0],
                      openweathermap: item.models.filter((el) => el.Provider === "Openweathermap")[0],
                      climacell: item.models.filter((el) => el.Provider === "Climacell")[0],
                      houroffset: item.houroffset,
                    });
                  }}
                />
              </>
            );
          }}
          horizontal={false}
          numColumns={2}
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    </>
  );
};
